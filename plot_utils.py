import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly
import csv
from scipy.optimize import minimize
from collections import namedtuple
import statistics as stats
import itertools as it
plot_res = 300 #dpi
sipms_area = 36 #mm^2 - four SiPMs
circuit_area = 4225 #mm^2
tape_reflectance = 0.08

# some utility functions
def is_iterable(item):
    try:
        check = (e for e in item)
        return True
    except TypeError as te:
        #print(item,'is not iterable')
        return False

def normalize_data(data):
    dists,signals = data
    ref = signals[np.argmax(dists)]
    result = [dists,[signal/ref for signal in signals]]
    return result

def scale_data(data,multiplier):
    #print('>>> data is',data,'of type',type(data))
    #print('>>> multiplier is',multiplier,'of type',type(multiplier))
    return [data[0],[multiplier*datum for datum in data[1]]]

            
def offset_data(data):
    dists,signals=data
    offset = dists[np.argmin(dists)]
    result = [[dist - offset for dist in dists],signals]
    return result

def read_sim(filename):
    sim = {}
    with open(filename,"r") as f:
        contents = f.readlines()
        percentage = ''
        insertions = []
        signals = []

        for line in contents:
            if len(line.split()) > 0 and line.split()[0].lower() == 'reflectivity:':
                if percentage != '':
                    sim[percentage] = [insertions,signals]
                    insertions = []
                    signals = []
                percentage = line.split()[1]
                continue
            elif len(line.split()) > 0 and line.split()[0].lower() != 'reflectivity':
                insertions.append(float(line.split()[0]))
                signals.append(float(line.split()[1]))
            else:
                continue
        # store last percentage data when you reach end of file
        sim[percentage] = [insertions,signals]
    return sim

def avg_sim(sim1,sim2,N=1e6):
    # N is the number of simulated events per point
    avgd_sim = {}
    sim_errs = {}
    syst_err_diffs = {}
    for key in sim1.keys():
        distances = sim1[key][0]
        curve1 = sim1[key][1]
        curve2 = sim2[key][1]
        avg_curve = [np.mean([v1,v2]) for v1,v2 in zip(curve1,curve2)]
        avgd_sim[key] = [distances,avg_curve]
        #print('>>> zip(curve1,curve2) is',list(zip(curve1,curve2)))
        #avg_err = [np.sqrt(((n1*(1-(n1/N))+n2*(1-(n2/N)))/2)+stats.stdev((n1,n2))**2) for n1,n2 in zip(curve1,curve2)]
        avg_err = [np.sqrt(((n1*(1-(n1/N))+n2*(1-(n2/N)))/2)+stats.stdev((n1,n2))**2) for n1,n2 in zip(curve1,curve2)]
        syst_err = [stats.stdev((n1,n2)) for n1,n2 in zip(curve1,curve2)]
        syst_err_diff = [avg-syst for avg,syst in zip(avg_err,syst_err)]
        syst_err_diffs[key] = [distances,avg_err] 
        sim_errs[key] = [distances,avg_err]
    return avgd_sim, sim_errs, syst_err_diffs




def plot_sim(simdata,labelprefix,points=True,errbars=False,
             which_pcts=None):
    for key in simdata.keys():
        if which_pcts:
            if key not in which_pcts:
                continue
        if points and not errbars:
            plt.plot(simdata[key][0],simdata[key][1],
                     label=labelprefix+' '+key,
                     marker='',linestyle='--')
        elif points and errbars:
            plt.errorbar(simdata[key][0],simdata[key][1],
                    yerr=np.sqrt(simdata[key][1]),label=labelprefix+' '+key,
                    marker='.',linestyle='None')
        elif errbars and not points:
            plt.errorbar(simdata[key][0],simdata[key][1],
                    yerr=np.sqrt(simdata[key][1]),label=labelprefix+' '+key,
                    marker='None',linestyle='None')
        else:
            print("You've specified an impossible configuration!")
    plt.legend()

def sum_data(datasets):
    """
    Given a list of datasets (datasets, output of read_data), return a list of
    datasets in which all four SiPMs have been summed over.
    """
    summed_data = []
    summedSignals = []
    summedErrs = []
    for i in range(len(datasets)):
        summed = []
        err = []
        for dist in range(len(datasets[i]['Distance (mm)'])):
            dataset_sum = 0
            dataset_err = 0
            for nSIPM in range(1,5):
                #print('>>> datasets[i] is',datasets[i])
                dataset_sum += datasets[i]['Int '+str(nSIPM)+' (nWb)'][dist]
                # divide errs by 1000 since they're in pWb
                # append squared err since we're adding in quadrature
                dataset_err += (datasets[i]['Poisson err '+str(nSIPM)+' (nWb)'][dist])**2
            summed.append(dataset_sum)
            err.append(np.sqrt(dataset_err))
        summed_data.append({'Distance (mm)' : datasets[i]['Distance (mm)'],
                            'Int (nWb)' : summed,
                            'Poisson err (nWb)' : err})
    return summed_data

def normalize_for_biasv(data,vbias,vops):
    """
    Given a set of data, the value of the bias voltage
    used in the setup, and a list of the operating voltages (Vbr + 3V) for the
    SiPMs (this function assumes four SiPMs), returns a dataset in which all of
    the signals have been scaled such that they represent the gain the SiPM
    would have had at Vbr + 3V.
    """
    scaled_data = []
    for dataset in data:
        scaled_dataset = dataset
        for i_sipm in range(0,4):
            key = 'Int '+str(i_sipm+1)+' (nWb)'
            original_signals_dict = dict(filter(lambda item: key in item[0], dataset.items()))
            original_signals = list(original_signals_dict.values())[0]
            original_key = list(original_signals_dict.keys())[0]
            scaled_signals = [(signal*3.)/(3.+vbias-vops[i_sipm]) for signal in original_signals]
            #if -np.inf in vops:
                #print('scaled_signals:',scaled_signals)
            scaled_dataset[key] = scaled_signals
        scaled_data.append(scaled_dataset)
    return scaled_data

def read_csv(filename,diode=['uv'],sipm=['four blue'],vbias=None,vops=None,N=1000,
        summing_data=True):
    """
    Given a filename, the bias voltage used in the measurement, a list of the 
    operating voltages for each SiPM, the number of measurements that were
    taken to get the mean pulse area and its standard deviation, return
    a list of dictionaries containing each dataset in the file, a list of the
    conditions under which each of those datasets was taken ('coated', 'uncoated'),
    and a list of the thickness of the box for each measurement ('thinnest', 'medium',
    'thickest'). If summing_data is set to True, the list of dictionaries will
    consist of data summed over all four SiPMs.
    """
    numDataSets = 0

    def trim(myList):
        while myList[-1] == '':
            myList = myList[:len(myList)-1]
        return myList

    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False


    dataNamesAll = []
    dataPointsAll = []
    conditionsAll = []
    configData = []
    sipmsAll = []
    diodesAll = []
    thicknessesAll = []
    dataDictAll = []
    syst1All = {}

    """
    THIS READER ASSUMES EACH MEASUREMENT LOOKS LIKE THIS:

    [metadata headers (sipm, leds, etc)]
    [metadata values]
    [blank row]
    [data headers (distance, etc)]
    {[data rows]}
    [blank row]

    """

    with open(filename, newline = '') as f:
        reader = list(csv.reader(f))
        for index, row in enumerate(reader):
            # Discover that we have a new curve
            if row[0] == 'SiPM':
                header = row
                config = reader[index+1]
                configurations = dict(zip(header, config))
                # Check to make sure we're looking at CM ready data
                if 'SKIP' in header:
                    continue
                elif (configurations['SiPM'].lower() in sipm and 
                        configurations['Diode'].lower() in diode
                        and 'SYST1' not in header and 'SYST2' not in header):
                    numDataSets += 1
                    configData.append(configurations)
                    thicknessesAll.append(configurations['Thickness'])
                    conditionsAll.append(configurations['Condition'])    
                    sipmsAll.append(configurations['SiPM'])
                    diodesAll.append(configurations['Diode'])
                    # Find the names of all the data types, and the data
                    scanCol = 0
                    dataNames = []
                    dataPoints = []
                    while reader[index+3][scanCol] != '':
                        dataNames.append(reader[index+3][scanCol].strip())
                        # Find all the data points
                        scanRow = 0
                        dataVecCol = []
                        #print('dataNames is',reader[index+4+scanRow])
                        while (index+4+scanRow) < len(reader) and is_number(reader[index+4+scanRow][scanCol]):
                            dataVecCol.append(float(reader[index+4+scanRow][scanCol]))
                            scanRow += 1
                        dataPoints.append(dataVecCol)
                        scanCol += 1
                    dataNamesAll.append(dataNames)
                    dataPointsAll.append(dataPoints)
                    dataDictAll.append(dict(zip(dataNames, dataPoints)))
                elif (configurations['SiPM'] == 'Four blue' #and configurations['Diode'] == diode
                        and 'SYST1' in header and 'SYST2' not in header):
                    currentkey = (configurations['Thickness'],configurations['Condition'])
                    systheader = reader[index+3]
                    systrow = reader[index+4]
                    systdict = dict(zip(systheader,systrow))
                    systerrs = []
                    for i_sipm in range(1,5):
                        systerrs.append(float(systdict['uncer '+str(i_sipm)]))
                    syst1All[currentkey] = systerrs
                else:
                    continue

    # Correct the "read distances" to "insertion distances"
    for i in range(numDataSets):
        #print('>>> dataDictAll[',i,'] is',dataDictAll[i])
        #print('>>> dataDictAll[',i,'][Distance (mm)] is',dataDictAll[i]['Distance (mm)'])
        offset = dataDictAll[i]['Distance (mm)'][np.argmin(dataDictAll[i]['Distance (mm)'])]
        for j in range(len(dataDictAll[i]['Distance (mm)'])):
            dataDictAll[i]['Distance (mm)'][j] -= offset

    # if given bias voltage and operating voltage information, normalize
    # everything to an overvoltage of 3V
    if vbias and vops:
        dataDictAll = normalize_for_biasv(dataDictAll,vbias,vops)

    # compute errors on the mean of the area measurement
    # (meas / sqrt(N))
    pwb_to_nwb = 1./1000. # since stdevs are given in pWb
    for i in range(len(dataDictAll)):
        for i_sipm in range(1,5):
            stdevs = dataDictAll[i]['Stdev '+str(i_sipm)+' (pWb)']
            errs_on_mean = [(elt*pwb_to_nwb)/np.sqrt(N) for elt in stdevs]
            dataDictAll[i]['Poisson err '+str(i_sipm)+' (nWb)'] = errs_on_mean

    # sum data and systs if needed
    if summing_data:
        dataDictAll = sum_data(dataDictAll)
        for key in syst1All.keys():
            systs = syst1All[key]
            systs = np.array(systs)
            quadrature = np.sqrt(np.sum(systs**2))
            syst1All[key] = quadrature
        
    return dataDictAll, conditionsAll, thicknessesAll, syst1All, sipmsAll, diodesAll, configData

def syst_from_frac_syst(frac_systs,data,thicknesses,conditions):
    out_systs = dict.fromkeys(frac_systs.keys())
    for idx, key in enumerate(zip(thicknesses,conditions)):
        key = (key[0].replace(' ',''),key[1].replace(' ',''))
        dataset = data[idx]
        this_frac_systs = frac_systs[key]
        nwb_systs = []
        for jdx,signal in enumerate(dataset['Int (nWb)']):
            nwb_systs.append(this_frac_systs[jdx]*signal)
        out_systs[key] = nwb_systs
    return out_systs

    
    

def compute_avg_slopes(dataset):
    avg_slopes = []
    intkey = 'Int (nWb)'
    for idx, dist in enumerate(dataset['Distance (mm)']):
        if idx == 0:
            slope = (dataset[intkey][idx+1]-dataset[intkey][idx])/(dataset['Distance (mm)'][idx+1]-dist)
            avg_slopes.append(slope)
        elif idx == (len(dataset['Distance (mm)']) - 1):
            slope = (dataset[intkey][idx]-dataset[intkey][idx-1])/(dist-dataset['Distance (mm)'][idx-1])
            avg_slopes.append(slope)
        else:
            slope1 = (dataset[intkey][idx+1]-dataset[intkey][idx])/(dataset['Distance (mm)'][idx+1]-dist)
            slope2 = (dataset[intkey][idx]-dataset[intkey][idx-1])/(dist-dataset['Distance (mm)'][idx-1])
            slope = 0.5*(slope1+slope2)
            avg_slopes.append(slope)
    return avg_slopes

def compute_chi2(alpha,dataset,single_MC,syst,xerr,exclude,single_simerr):
    """
    Given a scaling (alpha), a dataset (dataset, one element of the list of dictionaries
    produced by read_data), and an MC set (single_MC, one element from the dictionary
    produced by read_sim), return the reduced chi-squared of that ratio
    """
    #print('single_MC is',single_MC)
    single_MC = scale_data(single_MC,alpha)
    single_simerr = scale_data(single_simerr,alpha)
    chi2 = 0
    if not syst:
        syst = [0]*len(dataset['Distance (mm)'])
    #if is_iterable(syst):
    #    syst = syst[0]


    # what are the keys for the columns with the integrals and errors?
    if 'Poisson err (nWb)' in dataset.keys():
        errkey = 'Poisson err (nWb)'
        intkey = 'Int (nWb)'
    else:
        print(">>> WARNING: This is unsummed data. Results will be for SiPM 1 ONLY")
        errkey = 'Poisson err 1 (nWb)'
        intkey = 'Int 1 (nWb)'

    # calculate average slopes between points
    avg_slopes = []
    for idx, dist in enumerate(dataset['Distance (mm)']):
        if exclude:
            if idx in exclude:
                #print('>>> excluding',idx,'in average slope calculation')
                continue
        if idx == 0:
            slope = (dataset[intkey][idx+1]-dataset[intkey][idx])/(dataset['Distance (mm)'][idx+1]-dist)
            avg_slopes.append(slope)
        elif idx == (len(dataset['Distance (mm)']) - 1):
            slope = (dataset[intkey][idx]-dataset[intkey][idx-1])/(dist-dataset['Distance (mm)'][idx-1])
            avg_slopes.append(slope)
        else:
            slope1 = (dataset[intkey][idx+1]-dataset[intkey][idx])/(dataset['Distance (mm)'][idx+1]-dist)
            slope2 = (dataset[intkey][idx]-dataset[intkey][idx-1])/(dist-dataset['Distance (mm)'][idx-1])
            slope = 0.5*(slope1+slope2)
            avg_slopes.append(slope)

    for idx,dist in enumerate(dataset['Distance (mm)']):
        if exclude:
            if idx in exclude:
                #print('>>> excluding',idx)
                continue
        if dist not in single_MC[0]:
            print('>>> this distance was not in the MC set:',dist)
            print('>>> single_MC[0] is',single_MC[0])
            wait = input('press enter')
            continue
        else:
            #print('>>> adding',idx,'in chi2 calculation')
            #tape_uncert = dataset[intkey][idx]*(sipms_area/circuit_area)*tape_reflectance
            total_uncert = np.sqrt(dataset[errkey][idx]**2 + syst[idx]**2) #+ tape_uncert**2)
            if single_simerr:
                total_uncert = np.sqrt(total_uncert**2 + single_simerr[1][idx]**2)
            if xerr:
                converted_yerr = xerr*avg_slopes[idx]
                #print('>>> adding',converted_yerr,'to total error')
                total_uncert = np.sqrt(total_uncert**2 + converted_yerr**2) 
            dist_idx = single_MC[0].index(dist)
            #chi2 += ((dataset[intkey][idx] - single_MC[1][dist_idx])/dataset[errkey][idx])**2
            chi2 += ((dataset[intkey][idx] - single_MC[1][dist_idx])/total_uncert)**2
    return chi2

def simultaneous_chi2(alpha, datasets, MCs, syst, xerr, thicknesses, 
                    conditions, 
                    exclude,
                    simerr):
    """
    Computes the total chi-squared for a single alpha scaling multiple
    MC sets to fit multiple datasets simultaneously. Given a single alpha,
    a list of datasets, a list of corresponding MC curves, a list of 
    systematics, and a single x error, returns a chi squared value for
    those MC curves scaled by alpha to those datasets, incorporating the xerr.
    """
    total_chi2 = 0
    #print('>>> datasets is',datasets)
    #print('>>> simerr is',simerr)
    for idx,dataset in enumerate(datasets):
        #print('>>> Im in the simultaneous_chi2 loop')
        single_MC = MCs[idx]
        single_simerr = simerr[idx]
        #print('syst is',syst)
        this_syst = syst[(thicknesses[idx].replace(' ',''),conditions[idx].replace(' ',''))]
        #single_syst = systs[(thicknesses[i],conditions[i])]
        #single_syst = systs[idx]
        #print('>>> single_MC is',single_MC,'of type',type(single_MC))
        total_chi2 += compute_chi2(alpha,dataset,single_MC,
                                   this_syst,xerr,exclude,
                                   single_simerr)

    #print('>>> total_chi2 is',total_chi2)

    return total_chi2


def fit_ref_ratio(dataset,single_MC,initial_guess=0.01,syst=None,xerr=None,single_simerr=None):
    """
    Given a single dataset (one dict from the output of read_data) and
    a single set of MC (one value (a list of two lists) from the output of
    read_sim), return the multiplier for the MC curve which makes it best
    fit the dataset (float), and the chi^2 of that fit (float)
    """
    results = minimize(compute_chi2,initial_guess,
                        method='Nelder-Mead',
                        args=(dataset,single_MC,syst,xerr,None,single_simerr))
    ndf = len(dataset['Distance (mm)']) - 2
    chi2_over_ndf = results.fun/ndf
    return results.x[0], chi2_over_ndf

def simultaneous_fit_ref_ratio(datasets,all_MCs,selected_MCs,
        initial_guess=0.01, syst=None,xerr=None,
        thicknesses=None,conditions=None,exclude=None,
        simerr=None):
    """
    Given a list of datasets and a set of MC, return the multiplier for the 
    MC curves which makes them best fit the datasets (float), and the chi^2 of 
    that fit (float)
    """
    input_MCs = [all_MCs[key] for key in selected_MCs] 
    input_simerrs = [simerr[key] for key in selected_MCs]
    results = minimize(simultaneous_chi2,initial_guess,
                        method='Nelder-Mead',
                        args=(datasets,input_MCs,syst,xerr,
                              thicknesses,conditions,exclude,
                              input_simerrs))
    if exclude:
        ndf = ((len(datasets[0]['Distance (mm)'])-len(exclude))*len(datasets))-(len(datasets)+1)
    else:
        ndf = (len(datasets[0]['Distance (mm)'])*len(datasets)) - (len(datasets)+1)
    chi2_over_ndf = results.fun/ndf
    return results.x[0], chi2_over_ndf


def fit_reflectivity(dataset,MC,syst=None,xerr=None,simerr=None):
    """
    Given a single dataset (one dict from the output of read_data) and
    a set of MC runs (the output of read_sim), return two objects: a tuple
    containing the best fit reflectance (string), the ratio by which that 
    reflectance's curve must be multiplied to fit (float), and the reduced
    chi^2 of the fit (float); and a dict with reflectances (strings) as keys,
    and tuples containing the best-fit ratio and reduced chi^2 for each of those
    reflectances.
    """
    ratios = dict.fromkeys(MC.keys())
    chi2s = dict.fromkeys(MC.keys())
    all_fits = dict.fromkeys(MC.keys())
    for key in MC.keys():
        ratio, chi2 = fit_ref_ratio(dataset,MC[key],syst=syst,xerr=xerr,
                                    single_simerr=simerr[key])
        ratios[key] = ratio
        chi2s[key] = chi2
        all_fits[key] = (ratio, chi2)

    fitted_val = min(chi2s,key=chi2s.get)
    fitted_ratio = ratios[fitted_val]
    fitted_chi2 = chi2s[fitted_val]
    best_fit_tuple = (fitted_val, fitted_ratio, fitted_chi2)
    return best_fit_tuple, all_fits

def fit_all_data(data,MC,input_systs=None,xerr=None,thicknesses=None,
                 conditions=None,simerr=None):
    """
    Given data (the output of read_data) and MC (the output of read_sim),
    find the best fit reflectance to each dataset and return a list of the
    outputs of fit_reflectivity for each dataset.
    """
    fit_results = []
    all_fits_results = []

    # make a copy of input systs so you don't modify it globally
    systs = dict(input_systs)

    for i in range(len(data)):
        syst = None
        print('>>> systs is',systs)
        if systs:
            #syst = systs[(thicknesses[i],conditions[i])]
            syst = systs[(thicknesses[i].replace(' ',''),conditions[i].replace(' ',''))]
        fit_ref_result, all_fits = fit_reflectivity(data[i],MC,syst,xerr=xerr,simerr=simerr)
        fit_results.append(fit_ref_result)
        all_fits_results.append(all_fits)
    return fit_results, all_fits_results

def compute_ref_uncerts(all_fits_results,datasets,systematics,MC,xerr,thicknesses,
                        conditions,simerr):
    """
    Given a list of dicts with reflectances (strings) as keys and 
    tuples containing the best-fit ratio and reduced chi^2 as values,
    return a list of tuples containing the lower and upper bounds on
    the best fit (lowest chi-squared) reflectance.
    """
    bounds = []
    for dataset in all_fits_results:
        print('>>> dataset in all_fits_results is',dataset)
        sort_data = {k: v for k, v in sorted(dataset.items(),key=lambda x: x[1][1])}
        print('>>> sort_data in all_fits_results is',sort_data)
        min_alpha = sort_data[sort_data.keys()[0]][0]

        #for key in dataset.keys():
        #    xs = append(float(key))
        #    ys = 
        for key in dataset.keys():
            xs.append(float(key))
            ys.append(dataset[key][1])
        minval = min(ys)
        rescaled_ys = [y/minval for y in ys]
        #print('>>> rescaled_ys is',rescaled_ys)
        interp_xs = np.arange(min(xs),max(xs),0.0001)
        #print('>>> interp_xs is',interp_xs)
        interp_ys = np.interp(interp_xs,xs,rescaled_ys)
        #print('>>> interp_ys is',interp_ys)
        bound_range = [elt[0] for elt in zip(interp_xs,interp_ys) if elt[1] <= 2]
        #print('>>> bound_range is',bound_range)
        upperbound = max(bound_range)
        lowerbound = min(bound_range)
        bounds.append((lowerbound,upperbound))

    return bounds

def simultaneous_fit_data(datasets,MCs,input_systs,xerr,thicknesses,conditions,
        simerr=None,exclude=None):
    """
    Given a group of datasets which were taken using the same LED and SiPM
    configuration, a list of MC curves, a list of systematics, and an x error,
    fit reflectance curves to all datasets with the restriction
    that alpha will be shared between them. Returns a list of the best fit 
    reflectances, a list of the individual dataset chi squareds, the total
    chi squared, and the best fit alpha.
    """
    # there are (# of MC sets)**(# of datasets) possible combinations of
    # MC here. There may be a more efficient way of searching this space.
    
    # make a copy of input_systs so you don't modify it globally
    systs = dict(input_systs)


    # make a list of the possible MC combinations:
    all_mc_combos = list(it.product(MCs,repeat=len(datasets)))

    combo_chi2s = []
    #ratios = dict.fromkeys(list(all_mc_combos))
    #chi2s = dict.fromkeys(list(all_mc_combos))
    ratios = {}
    chi2s = {}
    for idx,MC_combo in enumerate(all_mc_combos):
        if idx % 100 == 0:
            print('>>> at combo',idx)
        results = simultaneous_fit_ref_ratio(datasets,MCs,MC_combo,syst=systs,
                xerr=xerr,thicknesses=thicknesses,conditions=conditions,
                exclude=exclude,simerr=simerr)
        ratios[MC_combo] = results[0]
        chi2s[MC_combo] = results[1]

    fitted_vals = min(chi2s,key=chi2s.get)
    fitted_ratio = ratios[fitted_vals]
    fitted_chi2 = chi2s[fitted_vals]
    best_fit_tuple = (fitted_vals, fitted_ratio, fitted_chi2)
    return best_fit_tuple, chi2s, ratios

def chi2_curves(fitresults,datasets,systematics,MC,xerr,thicknesses,
                conditions,simerr,exclude=None,usefitmin=False,
                fit_degree=4,interval=3):
    """
    Given the results of simultaneous_fit_data and the datasets, systematics, and 
    MC used in the fitting, return four chi2 curves in which a single fit 
    reflectance Ri is scanned through all possible values while alpha and the 
    other reflectances {Rj} are held fixed.
    """
    reflectances = sorted(list(MC.keys()),key=lambda x: float(x))
    float_reflectances = sorted([float(item) for item in MC.keys()])
    print('>>> reflectances is',reflectances)
    print('>>> fit_degree is',fit_degree)
    bestfits = fitresults[0]
    all_chi2s = fitresults[1]
    alpha = fitresults[0][1] # fitted_ratio in best_fit_tuple
    best1 = fitresults[0][0][0]
    best2 = fitresults[0][0][1]
    best3 = fitresults[0][0][2]
    best4 = fitresults[0][0][3]
    best_reflectances = fitresults[0][0]
    print('>>> best_reflectances is',best_reflectances)


    chi2_scans = []
    chi2_windows = []
    for idx,bestref in enumerate(best_reflectances):
        # make the list of reflectance keys
        all_uncombined_refs = [[best] for best in best_reflectances]
        all_uncombined_refs[idx] = reflectances
        print('>>> all_uncombined_refs is',all_uncombined_refs)
        all_combined_refs = list(it.product(*all_uncombined_refs))
        print('>>> all_combined refs is',all_combined_refs)
        all_refs_chi2s = [all_chi2s[key] for key in all_combined_refs]
        min_idx = np.argmin(all_refs_chi2s)

        uncombined_refs = [[best] for best in best_reflectances]
        fitting_window = reflectances[max(min_idx-interval,0):min(min_idx+interval+1,len(reflectances))]
        chi2_windows.append(fitting_window)
        uncombined_refs[idx] = fitting_window
        combined_refs = list(it.product(*uncombined_refs))
        these_chi2s = []

        for jdx,refcombo in enumerate(combined_refs):
            # calculate the chi2
            MCs = [MC[ref] for ref in refcombo]
            input_simerrs = [simerr[key] for key in reflectances]
            this_chi2 = simultaneous_chi2(alpha,datasets,MCs,systematics,xerr,
                              thicknesses,conditions,exclude,input_simerrs)
            #ndf = len(datasets[0]['Distance (mm)']) - 2
            ndf = (len(datasets[0]['Distance (mm)'])*len(datasets)) - (len(datasets)+1)
            this_chi2 = this_chi2/ndf
            these_chi2s.append(this_chi2)
#def simultaneous_chi2(alpha, datasets, MCs, syst, xerr, thicknesses, 
#                    conditions, 
#                    exclude,
#                    simerr):
        chi2_scans.append(these_chi2s)

    poly_fits = []
    scaled_scans = []
    for idx,scan in enumerate(chi2_scans):
        all_uncombined_refs = [[best] for best in best_reflectances]
        all_uncombined_refs[idx] = reflectances
        all_combined_refs = list(it.product(*all_uncombined_refs))
        all_refs_chi2s = [all_chi2s[key] for key in all_combined_refs]
        min_idx = np.argmin(all_refs_chi2s)

        these_float_reflectances = float_reflectances[max(0,min_idx-interval):min(min_idx+interval+1,len(float_reflectances))]
        #print('>>> len(these_float_reflectances) is',len(these_float_reflectances))
        # rescale scan so minimum is 1
        scanmin = min(scan)
        scaled_scan = [item/scanmin for item in scan]
        scaled_scans.append(scaled_scan)
        #print('>>> len(scaled_scan) is',len(scaled_scan))
        fit_poly = poly.polyfit(these_float_reflectances,scaled_scan,
                                                deg=fit_degree)
        poly_fits.append(fit_poly)
        #print('>>> polynomial coefficients are',fit_poly)

    return scaled_scans,poly_fits,chi2_windows




    #plot_xs = [float(item) for item in reflectances]

    #curve_xs = list(it.product(reflectances,[best2],[best3],[best4]))
    #curve_ys = []
    #for key in curve_xs:
    #    curve_ys.append(all_chi2s[key])
    #minval1 = min(curve_ys)
    #rescaled_ys1 = [y/minval1 for y in curve1_ys]
    #interp_ys1 = np.interp(interp_xs,plot_xs,rescaled_ys1)
    #bound_range1 = [elt[0] for elt in zip(interp_xs,interp_ys1) if elt[1] <= 2]
    #upperbound1 = max(bound_range1)
    #lowerbound1 = min(bound_range1)





def plot_simultaneous_fits(data,MC,fitresults,systs,xerr,thicknesses,
        conditions,title,simerr=None,ref_bounds=None,exclude=None,
        total_area=None,title_fontsize=20,label_fontsize=20,legend_fontsize=20,
        ticklabel_fontsize=20,infotext_fontsize=20,all_fontsizes=None,
        ylim=None,pad=None,legalpha=None,residuals=False,res_ylim=None):
    #print('>>> fitresults is',fitresults)
    winning_mcs = fitresults[0][0]
    winning_alpha = fitresults[0][1]
    winning_chi2 = fitresults[0][2]
    if residuals:
        ratio = (2,3)
        scale = 3.25
        fig, axs = plt.subplots(len(data)+1,1,
                                 sharex=True,
                                 gridspec_kw={'height_ratios': [4,1,1,1,1]},
                                 figsize=(scale*ratio[0],scale*ratio[1]))
        ax = axs[0]
        plt.sca(ax)
    else:
        ax = plt.gca()


    markers = ['o','s','^','X']
    linestyles = [':','-.','-','--']
    colors = []
    for dataset in data:
        color = next(ax._get_lines.prop_cycler)['color']
        colors.append(color)
    
    distances = []
    data_errs = []
    data_vals = []
    for idx,dataset in enumerate(data):
        if exclude:
            xs = list(np.delete(np.array(dataset['Distance (mm)']),exclude))
            ys = list(np.delete(np.array(dataset['Int (nWb)']),exclude))
        else:
            xs = dataset['Distance (mm)']
            ys = dataset['Int (nWb)']
        stat_err = dataset['Poisson err (nWb)']
        #print('>>> stat_err is',stat_err)
        #print('>>> from plot_utils: systs is',systs)
        syst_err = systs[(thicknesses[idx].replace(' ',''),conditions[idx].replace(' ',''))]
        #print('>>> syst_err is',syst_err)
        avg_slopes = compute_avg_slopes(dataset)
        #print('>>> avg_slopes is',avg_slopes)
        xtoyerr = xerr*np.array(avg_slopes)
        #print('>>> xtoyerr is',xtoyerr)
        total_err = [np.sqrt(stat**2+syst**2+xtoy**2) for stat,syst,xtoy in zip(stat_err,syst_err,xtoyerr)]
        #print('>>> total_err is',total_err)
        if total_area:
            #xs = [item/total_area for item in xs]
            ys = [item/total_area for item in ys]
            total_err = [item/total_area for item in total_err]
        data_errs.append(total_err)
        data_vals.append(ys)
        distances.append(xs)
        plt.errorbar(xs,ys,yerr=total_err,label='Data '+thicknesses[idx],
                linestyle='None',marker=markers[idx],capsize=6,color=colors[idx])

    mc_vals = []
    mc_errs = []
    for idx,mc_key in enumerate(winning_mcs):
        #print('>>> this mc_key is',mc_key)
        if exclude:
            ys = np.delete(np.array(MC[mc_key][1]),exclude)*winning_alpha
            xs = np.delete(np.array(MC[mc_key][0],exclude))
        else:
            ys = np.array(MC[mc_key][1])*winning_alpha
            xs = MC[mc_key][0]

        #MC_label = r'MC fit '+format(100*float(mc_key),'.1f')+'% '
        MC_label = r'MC fit $'+format(100*float(mc_key),'.1f')
        if ref_bounds:
            if not is_iterable(ref_bounds[idx]):
                MC_label = MC_label+'$+/-'+format(100*(ref_bounds[idx]),'.2f')+'%'
                #MC_label = MC_label+'^{+'+format(100*(ref_bounds[idx]),'.2f')+'}'
                #MC_label = MC_label+'_{-'+format(100*(ref_bounds[idx]),'.2f')+'}$%'
            #MC_label = MC_label+'(+'+format(100*(ref_bounds[idx][1]-float(winning_mcs[idx])),'.2f')
            #MC_label = MC_label+'/-'+format(100*(float(winning_mcs[idx])-ref_bounds[idx][0]),'.2f')+')'
            else:
                MC_label = MC_label+'^{+'+format(100*(ref_bounds[idx][1]-float(winning_mcs[idx])),'.2f')+'}'
                MC_label = MC_label+'_{-'+format(100*(float(winning_mcs[idx])-ref_bounds[idx][0]),'.2f')+'}$%'
        #MC_label = MC_label+', $\chi^{2}/\\nu=$'+format(winning_chi2,'.2f')
        #MC_label = MC_label+', $\\alpha^{-1}=$'+format(1./winning_alpha,'.0f')
        if total_area:
            #xs = [item/total_area for item in xs]
            ys = [item/total_area for item in ys]
        plt.plot(xs,ys,label=MC_label,linestyle=linestyles[idx],marker='',color=colors[idx])
        mc_vals.append(ys)
        if simerr:
            #print('>>> simerr is',simerr)
            this_simerr = np.array(simerr[str(mc_key)][1])*winning_alpha
            if total_area:
                this_simerr = np.array([item/total_area for item in this_simerr])
            mc_errs.append(this_simerr)
            plt.fill_between(xs,ys-this_simerr,ys+this_simerr,
                    color=colors[idx],alpha=0.2)

    chi2_label = '$\chi^{2}/\\nu=$'+format(winning_chi2,'.2f')
    if total_area:
        chi2_label = chi2_label+'\n $\\alpha^{-1}=$'+format(1./(winning_alpha/total_area),'.0f')
    else:
        chi2_label = chi2_label+'\n $\\alpha^{-1}=$'+format(1./(winning_alpha),'.0f')

    if all_fontsizes:
        title_fontsize=all_fontsizes
        label_fontsize=all_fontsizes
        legend_fontsize=all_fontsizes
        ticklabel_fontsize=all_fontsizes
        infotext_fontsize=all_fontsizes

    ax.text(0.7,0.1,chi2_label,transform=ax.transAxes,fontsize=infotext_fontsize)
    

    plt.grid()
    #plt.xlabel('Distance (mm)',fontsize=label_fontsize)
    if total_area:
        plt.ylabel('V$\cdot$ns / mm$^2$',fontsize=label_fontsize)
    else:
        plt.ylabel('Pulse area (nWb)',fontsize=label_fontsize)
    plt.title(title,fontsize=title_fontsize)
    if legalpha:
        plt.legend(prop={'size':legend_fontsize},framealpha=legalpha)
    else:
        plt.legend(prop={'size':legend_fontsize})
    ax.tick_params(axis="x", labelsize=ticklabel_fontsize)
    ax.tick_params(axis="y", labelsize=ticklabel_fontsize)
    if ylim:
        plt.ylim(ylim)
    if pad:
        pass
        #plt.tight_layout(pad=pad)
    else:
        pass
        #plt.tight_layout()

    # take the difference of the data lists with the MC lists
    diffs = []
    for dataset,mcset in zip(data_vals,mc_vals):
        diffset = np.subtract(dataset,mcset)
        diffs.append(diffset)
    #print('>>> diffs is',diffs)
    # propagate the uncertainties together
    diff_errs = []
    for data_err,mc_err in zip(data_errs,mc_errs):
        diff_err = [np.sqrt(x1**2 + x2**2) for x1,x2 in zip(data_err,mc_err)]
        diff_errs.append(diff_err)

    if residuals:
        print('>>> diffs is',diffs)
        print('>>> distances is',distances)
        print('>>> diff_errs is',diff_errs)
        print('>>> axs is',axs)
        for idx in range(len(diffs)):
            i = idx + 1
            print('>>> idx is',idx)
            color = colors[idx]
            axs[i].errorbar(distances[idx],diffs[idx],yerr=diff_errs[idx],
                            linestyle='None',marker='.',color=color,capsize=3)
            axs[i].plot([min(distances[idx])-10,max(distances[idx])+10],[0,0],linestyle=':',
                    color='tab:gray')
            if res_ylim:
                axs[i].set(ylim=res_ylim)
            axs[i].set(xlim=[min(distances[idx])-5,max(distances[idx])+5])
            axs[i].set_xlabel('Distance (mm)',fontsize=17)
            axs[i].set_ylabel(thicknesses[idx],fontsize=17)
            axs[i].tick_params(axis="x", labelsize=17)
            axs[i].tick_params(axis="y", labelsize=17)
            axs[i].label_outer()
    plt.tight_layout()


def plot_residuals(data,MC,fitresults,systs,xerr,thicknesses,
        conditions,simerr,total_area,exclude=None,ylim=None):
    # calculate total uncertainties on data
    distances = []
    data_errs = []
    data_vals = []
    for idx,dataset in enumerate(data):
        if exclude:
            xs = list(np.delete(np.array(dataset['Distance (mm)']),exclude))
            ys = list(np.delete(np.array(dataset['Int (nWb)']),exclude))
        else:
            xs = dataset['Distance (mm)']
            ys = dataset['Int (nWb)']
        stat_err = dataset['Poisson err (nWb)']
        #print('>>> stat_err is',stat_err)
        syst_err = systs[(thicknesses[idx].replace(' ',''),conditions[idx].replace(' ',''))]
        #print('>>> syst_err is',syst_err)
        avg_slopes = compute_avg_slopes(dataset)
        #print('>>> avg_slopes is',avg_slopes)
        xtoyerr = xerr*np.array(avg_slopes)
        #print('>>> xtoyerr is',xtoyerr)
        total_err = [np.sqrt(stat**2+syst**2+xtoy**2) for stat,syst,xtoy in zip(stat_err,syst_err,xtoyerr)]
        #print('>>> total_err is',total_err)
        if total_area:
            #xs = [item/total_area for item in xs]
            ys = [item/total_area for item in ys]
            total_err = [item/total_area for item in total_err]
        data_errs.append(total_err)
        data_vals.append(ys)
        distances.append(xs)
    #print('>>> distances is',distances)
    #print('>>> data_vals is',data_vals)
    #print('>>> data_errs is',data_errs)

    # create list of MC results corresponding to data
    # calculate MC result uncertainties
    winning_mcs = fitresults[0][0]
    winning_alpha = fitresults[0][1]

    mc_vals = []
    mc_errs = []
    for idx,mc_key in enumerate(winning_mcs):
        #print('>>> this mc_key is',mc_key)
        if exclude:
            ys = np.delete(np.array(MC[mc_key][1]),exclude)*winning_alpha
            xs = np.delete(np.array(MC[mc_key][0],exclude))
        else:
            ys = np.array(MC[mc_key][1])*winning_alpha
            xs = MC[mc_key][0]

        if total_area:
            #xs = [item/total_area for item in xs]
            ys = [item/total_area for item in ys]
        mc_vals.append(ys)
        if simerr:
            this_simerr = np.array(simerr[str(mc_key)][1])*winning_alpha
            if total_area:
                this_simerr = np.array([item/total_area for item in this_simerr])
            mc_errs.append(this_simerr)
    #print('>>> mc_vals is',mc_vals)
    #print('>>> mc_errs is',mc_errs)

    # take the difference of the data lists with the MC lists
    diffs = []
    for dataset,mcset in zip(data_vals,mc_vals):
        diffset = np.subtract(mcset,dataset)
        diffs.append(diffset)
    #print('>>> diffs is',diffs)
    # propagate the uncertainties together
    diff_errs = []
    for data_err,mc_err in zip(data_errs,mc_errs):
        diff_err = [np.sqrt(x1**2 + x2**2) for x1,x2 in zip(data_err,mc_err)]
        diff_errs.append(diff_err)
    #print('>>> diff_errs is',diff_errs)

    plt.errorbar(distances[0],diffs[0],yerr=diff_errs[0],linestyle='None',
                 marker='o')
    # iterate through MC and data meta-lists:
    #    - create a subplot, stacked under any existing ones
    #    - plot the relevant residuals
    #    - 
    fig, axs = plt.subplots(len(data))
    ax = fig.gca()
    for idx in range(len(diffs)):
        color = next(ax._get_lines.prop_cycler)['color']
        axs[idx].errorbar(distances[idx],diffs[idx],yerr=diff_errs[idx],
                          linestyle='None',marker='.',color=color,capsize=3)
        axs[idx].plot([min(distances[idx])-10,max(distances[idx])+10],[0,0],linestyle=':',
                color='tab:gray')
        if ylim:
            axs[idx].set(ylim=ylim)
        axs[idx].set(xlim=[min(distances[idx])-5,max(distances[idx])+5])
        axs[idx].set_xlabel('Distance (mm)',fontsize=17)
        axs[idx].set_ylabel(thicknesses[idx],fontsize=17)
        axs[idx].tick_params(axis="x", labelsize=17)
        axs[idx].tick_params(axis="y", labelsize=17)
        axs[idx].label_outer()
    plt.tight_layout()



def plot_fits(data,MC,fitresults,conditions,thicknesses,diodes,sipms,
        systs=None,xerr=1.,dataprefix='Data ',
        which_fits='best', which_conditions=None, which_thicknesses=None,
        label_contents=None,total_area=None):
    """
    Given a list of datasets (output of read_data), a set of MC (output of read_sim),
    a set of fit results for the data (second output of fit_all_data), a list of the conditions
    for each dataset, a list of the thicknesses for each dataset, a label prefix to 
    use for the data, whether to plot the best fit for each dataset ('best') or 
    all of the fits for each dataset ('all'), and optionally a list of conditions 
    ('coated', 'uncoated') and a list of thicknesses ('thinnest', 'medium', thickest')
    to plot, creates a plot of those datasets and their corresponding fits.
    """
    ax = plt.gca()
    # create a dict for markers and colors
    #markers = {'coated' : 'o',
    #           'uncoated' : '^'}
    markers = {'coated' : '',
               'uncoated' : ''}

    print('>>> fitresults in plot_fits is',fitresults)
    wait = input('press enter')
    uncerts = compute_ref_uncerts(fitresults)
    thicknesses = [elt.lower() for elt in thicknesses]
    unique_thicknesses = set(thicknesses)
    colors = None
    if len(unique_thicknesses) == 1:
        colors = []
        for thickness in thicknesses:
            color = next(ax._get_lines.prop_cycler)['color']
            colors.append(color)
    else:
        colors = {}
        for thickness in unique_thicknesses:
            color = next(ax._get_lines.prop_cycler)['color']
            colors[thickness] = color
        

    for idx, dataset in enumerate(data):
        if which_conditions:
            if conditions[idx] not in which_conditions:
                print(">>> conditions don't match, skipping")
                continue
        if which_thicknesses:
            if thicknesses[idx].lower() not in which_thicknesses:
                print(">>> thicknesses don't match, skipping")
                continue

        # plot the data
        sig_key = ''
        err_key = ''
        if 'Int (nWb)' in dataset.keys():
            sig_key = 'Int (nWb)'
            err_key = 'Poisson err (nWb)'
        else:
            print(">>> WARNING: This is unsummed data. Results will be for SiPM 1 ONLY")
            sig_key = 'Int 1 (nWb)'
            err_key = 'Poisson err 1 (nWb)'

        syst = 0
        print('>>> systs is',systs)
        if systs:
            syst = systs[(thicknesses[idx].replace(' ',''),conditions[idx].replace(' ',''))]
            #print('>>> nonzero systs:',syst)
            #if is_iterable(syst):
            #    syst = syst[0]
        print('>>> syst is',syst)

        avg_slopes = compute_avg_slopes(dataset)
        xtoyerr = xerr*np.array(avg_slopes)
        errbars = [np.sqrt(err**2 + syst_val**2+xtoy**2) for err,syst_val,xtoy in 
                                                                zip(dataset[err_key],
                                                                    syst,xtoyerr)]
        #for jdx,err in enumerate(errbars):
        #    #tape_uncert = dataset[sig_key][jdx]*(sipms_area/circuit_area)*tape_reflectance
        #    #print('>>> adding tape uncert',tape_uncert)
        #    errbars[jdx] = np.sqrt(errbars[jdx]**2 + tape_uncert**2)
            
        #print('>>> errbars are',errbars)
        data_label = ''
        if label_contents:
            for item in label_contents:
                if item == 'thickness':
                    data_label += thicknesses[idx]+' '
                elif item == 'condition':
                    data_label += conditions[idx]+' '
                elif item == 'led':
                    print('diode is',diodes[idx])
                    data_label += diodes[idx]+' LED '
                elif item == 'sipm':
                    print('sipm is',sipms[idx])
                    data_label += sipms[idx]+' SiPMs '
                else:
                    print('>>> Unknwon label contents spec:',item)
        # decide if we're coloring by thickness
        this_color = None
        if len(unique_thicknesses) == 1:
            this_color = colors[idx]
        else:
            this_color = colors[thicknesses[idx]]

        ys = dataset[sig_key]
        if total_area:
            ys = [item/total_area for item in ys]
            errbars = [item/total_area for item in errbars]
            

        print('>>> plotting',sig_key)
        plt.errorbar(dataset['Distance (mm)'], ys,
                    #yerr=dataset[err_key], 
                    yerr=errbars, 
                    marker=markers[conditions[idx]],
                    linestyle='None',
                    #markersize=1,
                    color=this_color,
                    label=dataprefix+data_label,
                    capsize=3)

        # colors correspond to thicknesses
        # markers correspond to coated/uncoated
        # plot the fit(s)
        if which_fits == 'best':

            # decide if we're coloring by thickness
            this_color = None
            if len(unique_thicknesses) == 1:
                this_color = colors[idx]
            else:
                this_color = colors[thicknesses[idx]]

            all_fits = fitresults[idx]
            fit_uncerts = uncerts[idx]
            best_fit = min(all_fits.items(), key=lambda x: x[1][1])
            best_fit_ratio = best_fit[1][0]
            best_fit_chi2 = best_fit[1][1]
            best_fit_ref = best_fit[0]
            

            MC_scaled = scale_data(MC[best_fit_ref], best_fit_ratio)
            print('>>> MC_scaled is',MC_scaled)
            if total_area:
                MC_scaled[1] = [item/total_area for item in MC_scaled[1]]
            dist = dataset['Distance (mm)'][0]
            dist_idx = MC_scaled[0].index(dist)
            MC_label = r'MC fit '+format(100*float(best_fit_ref),'.1f')
            MC_label = MC_label+'$^{+'+format(100*(fit_uncerts[1]-float(best_fit_ref)),'.2f')+'}'
            MC_label = MC_label+'_{-'+format(100*(float(best_fit_ref)-fit_uncerts[0]),'.2f')+'}$%'
            MC_label = MC_label+', $\chi^{2}/\\nu=$'+format(best_fit_chi2,'.1f')
            if total_area:
                MC_label = MC_label+', $\\alpha^{-1}=$'+format(1./(best_fit_ratio/total_area),'.0f')
            else:
                MC_label = MC_label+', $\\alpha^{-1}=$'+format(1./best_fit_ratio,'.0f')
            plt.plot(MC_scaled[0], MC_scaled[1], 
                     label = MC_label,
                     marker='None', linestyle='--', 
                     color=this_color)
            print('>>> plotted fit',MC_label)
        elif which_fits == 'all':
            all_fits = fitresults[idx]
            for key in all_fits.keys():
                fit_ratio = all_fits[key][0]
                fit_chi2 = all_fits[key][1]
                
                MC_scaled = scale_data(MC[key], fit_ratio)
                plt.plot(MC_scaled[0], MC_scaled[1], 
                        label=r'MC fit '+key+' '+conditions[idx]+' '+thicknesses[idx]+', $\chi^{2}/\\nu=$'+format(fit_chi2,'.0f')+', $\\alpha^{-1}=$'+format(1./fit_ratio,'.0f'),
                        marker='None', linestyle='--')
        elif not isinstance(which_fits,str):
            all_fits = fitresults[idx]
            for pct in which_fits:
                if pct in all_fits.keys():
                    fit_ratio = all_fits[pct][0]
                    fit_chi2 = all_fits[pct][1]
                    MC_scaled = scale_data(MC[pct], fit_ratio)
                    plt.plot(MC_scaled[0], MC_scaled[1], 
                            label=r'MC fit '+pct+', $\chi^{2}/\\nu=$'+format(fit_chi2,'.0f')+', $\\alpha^{-1}=$'+format(1./fit_ratio,'.0f'),
                            marker='None', linestyle='--')
                else:
                    print('>>> '+pct+' was not found within this MC set. Skipping...')
        else:
            print('>>> Unknown value for which_fits:',which_fits)
    handles, labels = ax.get_legend_handles_labels()
    print('>>> handles are',handles)
    print('>>> labels are',labels)
    #if len(labels) > 0 and len(handles) > 0:
    #    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    if total_area:
        plt.ylabel('V$\cdot$ns / mm$^2$')
    else:
        plt.ylabel('Pulse area (nWb)')
    plt.xlabel('Distance (mm)')
    return labels,handles



def plot_data(data,conditions,thicknesses,diodes,sipms,systs=None,xerrs=None,
              which_conditions=None, which_thicknesses=None,which_diodes=None,
              which_sipms=None,ylims=None,label_contents=None):
    """
    Given all data (output of read_data), conditions and 
    thicknesses (output of read_data), plot all of the data. Optionally,
    specify the conditions (coated, uncoated) and thicknesses
    (thinnest, medium, thickest, 1cm) you want plotted. 
    """
    ax = plt.gca()
    # create a dict for markers and colors
    #markers = {'coated' : '.',
    #           'uncoated' : '.'}
    markers = {'coated' : '.',
               'uncoated' : ''}

    thicknesses = [elt.lower() for elt in thicknesses]
    unique_thicknesses = set(thicknesses)
    colors = {}
    for thickness in unique_thicknesses:
        color = next(ax._get_lines.prop_cycler)['color']
        colors[thickness] = color


    for i in range(len(data)):
        if which_conditions:
            if conditions[i] not in which_conditions:
                continue
        if which_thicknesses:
            if thicknesses[i] not in which_thicknesses:
                continue
        if which_diodes:
            if diodes[i] not in which_diodes:
                continue
        if which_sipms:
            if sipms[i] not in which_sipms:
                continue

        int_idxs = []
        err_idxs = []
        if 'Poisson err (nWb)' in data[i].keys():
            err_idxs.append('Poisson err (nWb)')
            int_idxs.append('Int (nWb)')
        else:
            for i_sipm in range(1,5):
                err_idxs.append('Poisson err '+str(i_sipm)+' (nWb)')
                int_idxs.append('Int '+str(i_sipm)+' (nWb)')

        syst = [0]*len(err_idxs)
        if systs:
            syst = systs[(thicknesses[i],conditions[i])]
            if not is_iterable(syst):
                syst = [syst]
        #print('>>> syst is',syst)

        for number, idxs in enumerate(zip(int_idxs,err_idxs)):
            if len(err_idxs) == 1:
                # the one scenario in which we don't want data
                # from the same thickness box to have the same color
                # is when we're doing coated/uncoated comparisons - so when
                # which_thicknesses >= 1
                if is_iterable(which_thicknesses) and len(which_thicknesses) >= 1:
                    point_color = next(ax._get_lines.prop_cycler)['color'] 
                else:
                    point_color = colors[thicknesses[i]]
                data_label = ''
                if label_contents:
                    data_label = ''
                    for item in label_contents:
                        if item == 'thickness':
                            data_label += thicknesses[i]+' thick '
                        elif item == 'condition':
                            data_label += conditions[i]+' '
                        elif item == 'led':
                            data_label += diodes[i]+' LED '
                        elif item == 'sipm':
                            data_label += sipms[i]+' SiPMs '
                        else:
                            print('>>> Unknown label contents spec:',item)
            else:
                # in this case we know we're doing individual SiPMs
                data_label= 'SiPM '+str(number+1)
                point_color = next(ax._get_lines.prop_cycler)['color']

            #print('>>> syst[number] is',syst[number])
            err_idx = idxs[1]
            int_idx = idxs[0]
            syst_err = syst[number]
            #print('>>> data[i][err_idx] is',data[i][err_idx])
            #print('>>> syst_err is',syst_err)
            errorbars = [np.sqrt(err**2 + syst_err**2) for err in data[i][err_idx]]
            #print('>>> errorbars is',errorbars)


            #print('>>> plotting',thicknesses[i])
            #ax.errorbar(data[i]['Distance (mm)'],data[i][int_idx],xerr=xerrs,
            #         #yerr=data[i][err_idx],
            #         yerr=errorbars,
            #         label=data_label,
            #         #markersize=1,
            #         capsize=3,
            #         color=point_color,marker=markers[conditions[i]],
            #         linestyle='None')
            ax.errorbar(data[i]['Distance (mm)'],data[i][int_idx],#xerr=xerrs,
                     #yerr=data[i][err_idx],
                     yerr=errorbars,
                     label=data_label,
                     #markersize=1,
                     capsize=3,
                     color=point_color,marker=markers[conditions[i]],
                     linestyle='None')
        if ylims:
            plt.ylim(ylims)
    handles, labels = ax.get_legend_handles_labels()
    # sort both labels and handles by labels
    if len(labels) > 0 and len(handles) > 0:
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    return labels,handles

def chi2_curve(fits,thicknesses,conditions,leds,sipms,subfolder=None,
        show_range=False):
    ranges = None
    if show_range:
        ranges = compute_ref_uncerts(fits)
    for index, fit_set in enumerate(fits):
        plt.figure()
        ax = plt.axes()
        thickness = thicknesses[index]
        condition = conditions[index]
        led_color = leds[index]
        sipm_config = sipms[index]
        percentages = []
        chi2s = []
        for key in fit_set.keys():
            percentages.append(float(key))
            chi2s.append(fit_set[key][1])
        if show_range and ranges:
            minchi2 = min(chi2s)
            chi2s = [chi/minchi2 for chi in chi2s]
        plt.plot(percentages,chi2s,marker='o',linestyle='--')
        if show_range and ranges:
            two_range = np.arange(ranges[index][0],ranges[index][1],0.0001)
            plt.plot(two_range,2*np.ones(len(two_range)),marker='',linestyle='-',color='r')
            ymin, ymax = ax.get_ylim()
            frac = (2. - ymin)/(ymax - ymin)
            plt.axvline(x=ranges[index][0],ymax=frac,color='r')
            plt.axvline(x=ranges[index][1],ymax=frac,color='r')

        plt.title(r'$\chi^2$ vs fit reflectance for '+thickness + ' ' + condition + ' ('+led_color+','+sipm_config+')')
        plt.xlabel('Reflectance')
        plt.ylabel(r'$\chi^2$')
        if subfolder:
            subfolder = subfolder + '/'
        else:
            subfolder = ''
        plt.savefig(subfolder+'chi2_curve_'+thickness+'_'+condition+'_'+led_color+'_'+sipm_config+'.png')
        plt.close()

#def simultaneous_chi2_curve(fits,thicknesses,conditions,leds,sipms,subfolder=None,
#        show_range=False):
#    # THIS CURRENTLY ASSUMES YOU'RE ONLY FITTING FOUR BOXES
#    # NO FEWER AND NO MORE THAN FOUR
#    reflectances = ['0.9','0.91','0.92','0.93','0.94','0.95','0.96','0.97','0.98',
#            '0.99']
#    bestfits = fits[0]
#    all_chi2s = fits[1]
#    best1 = fits[0][0][0]
#    best2 = fits[0][0][1]
#    best3 = fits[0][0][2]
#    best4 = fits[0][0][3]
#
#    plot_xs = [float(item) for item in reflectances]
#
#    curve1_xs = list(it.product(reflectances,[best2],[best3],[best4]))
#    curve1_ys = []
#    for key in curve1_xs:
#        curve1_ys.append(all_chi2s[item])
#    plt.figure()
#    plt.plot(plot_xs,curve1_ys)
#    plt.title(r'$\chi^2$ vs fit reflectance for '+thickness + ' ' + condition + ' ('+led_color+','+sipm_config+')')
#    plt.xlabel('Reflectance')
#    plt.ylabel(r'$\chi^2$')
#    plt.figure(
#
#    curve2_xs = list(it.product([best1],reflectances,[best3],[best4]))
#    curve2_ys = []
#    for key in curve2_xs:
#        curve2_ys.append(all_chi2s[item])
#
#    curve3_xs = list(it.product([best1],[best2],reflectances,[best4]))
#    curve3_ys = []
#    for key in curve3_xs:
#        curve3_ys.append(all_chi2s[item])
#
#    curve4_xs = list(it.product([best1],[best2],[best3],reflectances))
#    curve4_ys = []
#    for key in curve4_xs:
#        curve4_ys.append(all_chi2s[item])
#
#
#        
#
#    ranges = None
#    if show_range:
#        ranges = compute_ref_uncerts(fits)
#    for index, fit_set in enumerate(fits):
#        plt.figure()
#        ax = plt.axes()
#        thickness = thicknesses[index]
#        condition = conditions[index]
#        led_color = leds[index]
#        sipm_config = sipms[index]
#        percentages = []
#        chi2s = []
#        for key in fit_set.keys():
#            percentages.append(float(key))
#            chi2s.append(fit_set[key][1])
#        if show_range and ranges:
#            minchi2 = min(chi2s)
#            chi2s = [chi/minchi2 for chi in chi2s]
#        plt.plot(percentages,chi2s,marker='o',linestyle='--')
#        if show_range and ranges:
#            two_range = np.arange(ranges[index][0],ranges[index][1],0.0001)
#            plt.plot(two_range,2*np.ones(len(two_range)),marker='',linestyle='-',color='r')
#            ymin, ymax = ax.get_ylim()
#            frac = (2. - ymin)/(ymax - ymin)
#            plt.axvline(x=ranges[index][0],ymax=frac,color='r')
#            plt.axvline(x=ranges[index][1],ymax=frac,color='r')
#
#        plt.title(r'$\chi^2$ vs fit reflectance for '+thickness + ' ' + condition + ' ('+led_color+','+sipm_config+')')
#        plt.xlabel('Reflectance')
#        plt.ylabel(r'$\chi^2$')
#        if subfolder:
#            subfolder = subfolder + '/'
#        else:
#            subfolder = ''
#        plt.savefig(subfolder+'chi2_curve_'+thickness+'_'+condition+'_'+led_color+'_'+sipm_config+'.png')
#        plt.close()

def syst_scan(dataset,MC,systlimits,step=0.01):
    systs = np.arange(systlimits[0],systlimits[1],step)
    chi2s = []
    fit_refs = []
    for syst in systs:
        bestfit = fit_reflectivity(dataset,MC,syst)
        chi2s.append(bestfit[0][2])
        fit_refs.append(bestfit[0][0])
    return systs, chi2s, fit_refs

def get_dataset(data,thicknesses,conditions,
                which_thickness,which_condition):
    for i in range(len(data)):
        if (thicknesses[i] == which_thickness) and (conditions[i] == which_condition):
            return data[i]
        else:
            continue
    print('>>> no dataset matched your specifications')

