import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy.optimize import minimize
from collections import namedtuple
import pickle
import itertools as it
from os import path
# append the parent directory to the python path...
sys.path.append(os.path.dirname(sys.path[0]))
# ...to enable importing plot_utils
import plot_utils as ps

plot_res = 300 #dpi

##### ESSENTIAL ANALYSIS INPUTS START HERE ######
# define the bias voltage used + vops for the four SiPMs
bias_v = 54.6 #V
blue_v_brs = [54.98, 54.52, 54.76, 54.69]
uv_v_ops = [-np.inf,55.95,56.34,56.40]

systs = {('0.5cm', 'coated'): 0.38078865530144873, 
         ('0.24cm', 'coated'): 0.58878405773814213, 
         ('0.16cm', 'coated'): 0.4281744193332806, 
         ('1cm', 'coated'): 0.4991659710463639, 
         ('1cm', 'uncoated'): 0.2415229457693566, 
         ('0.24cm', 'uncoated'): 0.24664414307450463, 
         ('0.16cm', 'uncoated'): 0.15811388301227491, 
         ('0.5cm', 'uncoated'): 0.15000000000207628,
         ('0.6cm', 'uncoated'): 0.168,
         ('0.8cm', 'uncoated'): 0.205}

plot_dir = 'eplastics_2mm_bsipm_bled_avg_sim'
data_dir = 'measured_data'

all_data, all_conditions, all_thicknesses, all_systs, all_sipms, all_diodes, all_config = ps.read_csv('../'+'/eplastics.csv',
						diode=['blue c','uv'],
						sipm=['4blue','3uv - 1blue'],
						vbias = bias_v,
						vops = blue_v_brs,
						summing_data = True)

#ub_data, ub_conditions, ub_thicknesses, ub_systs, ub_sipms, ub_diodes, ub_config = ps.read_csv('ub_eplastics.csv',
#						diode=['uv'],
#						sipm=['4blue','3uv - 1blue'],
#						vbias = bias_v,
#						vops = blue_v_brs,
#						summing_data = True)

ub_data, ub_conditions, ub_thicknesses, ub_systs, ub_sipms, ub_diodes, ub_config = ps.read_csv('uv_sipm_blue_led.csv',
						diode=['blue'],
						sipm=['3uv - 1 blue'],
						vbias = bias_v,
						vops = uv_v_ops,
						summing_data = True)

# sipm, led
bb_systs = {('1 cm', 'uncoated'): [0.429,0.281,0.363,0.482,0.514,0.535,0.821,1.409],
            ('8 mm', 'uncoated'): [0.381,0.376,0.236,0.242,0.335,0.374,0.424,0.961],
            ('6 mm', 'uncoated'): [0.429,0.297,0.271,0.204,0.258,0.386,0.434,0.935],
            ('5 mm', 'uncoated'): [0.298,0.275,0.326,0.293,0.416,0.374,0.373,1.039]}


#print('all_systs is',all_systs)
#print('all_data is',all_data)
#print('ub_systs is',ub_systs)

ub_frac_systs = dict.fromkeys(bb_systs.keys())
for idx, key in enumerate(zip(all_thicknesses,all_conditions)):
    bb_dataset = all_data[idx]
    this_bb_systs = bb_systs[key]
    frac_systs = []
    for jdx,signal in enumerate(bb_dataset['Int (nWb)']):
        frac_systs.append(this_bb_systs[jdx]/signal)
    ub_frac_systs[key] = frac_systs

ub_frac_systs = pickle.load(open('./systematics/bb_frac_systs.pkl','rb'))
ub_systs = ps.syst_from_frac_syst(ub_frac_systs,ub_data,
                                  ub_thicknesses,
                                  ub_conditions)
bb_systs = pickle.load(open('./systematics/bb_systs.pkl','rb'))

simerrscale = 2.
bb_simerrs = {}
for key in bb_systs.keys():
    syst = bb_systs[key]
    new_syst = []
    for item in syst:
        new_syst.append(item*simerrscale)
    bb_simerrs[key] = new_syst

agg_avg_syst = []
for key in bb_systs.keys():
    syst = bb_systs[key]
    for item in syst:
        agg_avg_syst.append(item)

print('>>> About to take a mean')
avg_d_syst = np.mean(np.array(agg_avg_syst))
print('>>> took a mean')
single_bb_simerrs = {}
for key in bb_systs.keys():
    single_bb_simerrs[key] = avg_d_syst*np.ones(len(bb_systs[key]))

ub_simerrs = {}
print('ub_systs is',ub_systs)
for key in ub_systs.keys():
    syst = ub_systs[key]
    new_syst = []
    for item in syst:
        new_syst.append(item*simerrscale)
    ub_simerrs[key] = new_syst


agg_avg_syst = []
for key in ub_systs.keys():
    syst = ub_systs[key]
    for item in syst:
        agg_avg_syst.append(item)

avg_d_syst = np.mean(np.array(agg_avg_syst))
single_ub_simerrs = {}
for key in ub_systs.keys():
    single_ub_simerrs[key] = avg_d_syst*np.ones(len(ub_systs[key]))
    

#### ESSENTIAL ANALYSIS INPUTS END HERE ######

## read in simulation data
uv_sim = ps.read_sim('simulation/lambertian_1mil_allpcts_uv/outuv.dat')
blue_sim = ps.read_sim('simulation/lambertian_1mil_allpcts/outblue.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/blue_profiler_raisedSiPMs_1mil.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/near_far_blue_2mm_test_1M.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/blue_conserved_profile_1mil.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/uv_gen_and_match_1mil.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/custom_profile_blue_a_0p1in_0p3in_1mil.dat')
all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/double_resolution_blue_a_0p1in_0p3in_90s_1mil.dat')
sim2 = ps.read_sim('simulation/consolidated_results/consolidated_results/double_resolution_blue_c_0p1in_0p3in_90s_1mil.dat')
#all_sim = ps.read_sim('simulation/test_exclusion.dat')
#sim2 = ps.read_sim('simulation/test_exclusion_2.dat')
avg_sim, simerr, whatever = ps.avg_sim(all_sim,sim2,1e6)
print('>>> all_sim is',all_sim)
print('>>> sim2 is',sim2)
print('>>> avg_sim is',avg_sim)
print('>>> simerr is',simerr)
#sim_100m = ps.read_sim('simulation/consolidated_results/

#def plot_sim(simdata,labelprefix,points=True,errbars=False):
plt.figure()
ps.plot_sim(all_sim,'Lambertian')
plt.xlabel('Distance (mm)')
plt.ylabel('# of photons detected')
plt.title('Simulated UV LED (2mm collimated) reflectance curves')
plt.legend(prop={'size':6})
plt.savefig(plot_dir+'/uv_2mm_profiler.png',dpi=300)
plt.close()


#uv_v_brs = [53.34, 52.95, 53.40, 54.52] # note that #4 is a blue SiPM

# read in real data
#all_data, all_conditions, all_thicknesses, all_systs, all_sipms, all_diodes, all_config = ps.read_csv('eplastics.csv',
#						diode=['blue c','uv'],
#						sipm=['4blue','3uv - 1blue'],
#						vbias = bias_v,
#						vops = blue_v_brs,
#						summing_data = True)

labels, handles = ps.plot_data(ub_data,ub_conditions,ub_thicknesses,
        ub_diodes,ub_sipms,label_contents=['thickness'])
plt.legend(handles,labels)
plt.savefig(plot_dir+'/data_comparison.png')

print('>>> bb_simerrs is',bb_simerrs)
results = []
if path.exists(plot_dir+'/fit_results.pkl'):
    results = pickle.load(open(plot_dir+'/fit_results.pkl', 'rb'))
else:
    results = ps.simultaneous_fit_data(all_data,all_sim,
                                       dict(bb_systs),
                                       1.,
                                       all_thicknesses,
                                       all_conditions,
                                       simerr=simerr)
    pickle.dump(results,open(plot_dir+'/fit_results.pkl','wb'))
#other_results = ps.simultaneous_fit_data(all_data,all_sim,bb_systs,1.,
#        all_thicknesses,all_conditions,distances=[210.,180.,150.,120.])
#pickle.dump(other_results,open(plot_dir+'/bb_simultaneous_results_last4.pkl','wb'))

#reflectances=['0.9','0.91']
#reflectances=['0.9','0.91','0.92','0.93','0.94','0.95','0.96','0.97','0.98','0.99']
reflectances=['0.9','0.905','0.91','0.915','0.92','0.925','0.93','0.935',
              '0.94','0.945','0.95','0.955','0.96','0.965','0.97','0.975',
              '0.98','0.985','0.99','0.995']
bestfits = results[0]
all_chi2s = results[1]
best1 = results[0][0][0]
best2 = results[0][0][1]
best3 = results[0][0][2]
best4 = results[0][0][3]

plot_xs = [float(item) for item in reflectances]
interp_xs = np.arange(min(plot_xs),max(plot_xs),0.0001)

curve1_xs = list(it.product(reflectances,[best2],[best3],[best4]))
curve1_ys = []
for key in curve1_xs:
    curve1_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve1_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[0] + ' ' + all_conditions[0] + ' ('+all_diodes[0]+','+all_sipms[0]+')')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[0]+'.png',dpi=300)
minval1 = min(curve1_ys)
rescaled_ys1 = [y/minval1 for y in curve1_ys]
interp_ys1 = np.interp(interp_xs,plot_xs,rescaled_ys1)
bound_range1 = [elt[0] for elt in zip(interp_xs,interp_ys1) if elt[1] <= 2]
upperbound1 = max(bound_range1)
lowerbound1 = min(bound_range1)

curve2_xs = list(it.product([best1],reflectances,[best3],[best4]))
curve2_ys = []
for key in curve2_xs:
    curve2_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve2_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[1] + ' ' + all_conditions[1] + ' ('+all_diodes[1]+','+all_sipms[1]+')')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[1]+'.png',dpi=300)
minval2 = min(curve2_ys)
rescaled_ys2 = [y/minval2 for y in curve2_ys]
interp_ys2 = np.interp(interp_xs,plot_xs,rescaled_ys2)
bound_range2 = [elt[0] for elt in zip(interp_xs,interp_ys2) if elt[1] <= 2]
upperbound2 = max(bound_range2)
lowerbound2 = min(bound_range2)

curve3_xs = list(it.product([best1],[best2],reflectances,[best4]))
curve3_ys = []
for key in curve3_xs:
    curve3_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve3_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[2] + ' ' + all_conditions[2] + ' ('+all_diodes[2]+','+all_sipms[2]+')')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[2]+'.png',dpi=300)
minval3 = min(curve3_ys)
rescaled_ys3 = [y/minval3 for y in curve3_ys]
interp_ys3 = np.interp(interp_xs,plot_xs,rescaled_ys3)
bound_range3 = [elt[0] for elt in zip(interp_xs,interp_ys3) if elt[1] <= 2]
upperbound3 = max(bound_range3)
lowerbound3 = min(bound_range3)

curve4_xs = list(it.product([best1],[best2],[best3],reflectances))
curve4_ys = []
for key in curve4_xs:
    curve4_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve4_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[3] + ' ' + all_conditions[3] + ' ('+all_diodes[3]+','+all_sipms[3]+')')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[3]+'.png',dpi=300)
minval4 = min(curve4_ys)
rescaled_ys4 = [y/minval4 for y in curve4_ys]
interp_ys4 = np.interp(interp_xs,plot_xs,rescaled_ys4)
bound_range4 = [elt[0] for elt in zip(interp_xs,interp_ys4) if elt[1] <= 2]
upperbound4 = max(bound_range4)
lowerbound4 = min(bound_range4)

all_ref_bounds = [(lowerbound1,upperbound1),
                  (lowerbound2,upperbound2),
                  (lowerbound3,upperbound3),
                  (lowerbound4,upperbound4)]


#plt.figure()
#title = 'Blue SiPMs with Blue LED'
#print('>>> bb_systs is:',bb_systs)
#ps.plot_simultaneous_fits(all_data,all_sim,results,bb_systs,1.,all_thicknesses,
#        all_conditions,title,simerr=2*avg_d_syst,ref_bounds=all_ref_bounds)
#plt.savefig(plot_dir+'/bsipm_bled_iteration_a.png',dpi=300)
#plt.close()

plt.figure()
total_area = 27 #mm^2
ordinary_fontsize = 17
print('>>> all_systs is',all_systs)
ps.plot_simultaneous_fits(all_data,avg_sim,results,bb_systs,1.,
        all_thicknesses,all_conditions,'',#title,
        simerr=simerr,ref_bounds=all_ref_bounds,total_area=total_area,
        #title_fontsize=15,label_fontsize=15,all_fontsizes=15,ylim=[1.1,5])
        title_fontsize=ordinary_fontsize,label_fontsize=ordinary_fontsize,
        legend_fontsize=14,ticklabel_fontsize=ordinary_fontsize,
        infotext_fontsize=ordinary_fontsize,pad=0.3,legalpha=0.4,residuals=True,res_ylim=[-0.1,0.1])
plt.savefig(plot_dir+'/bsipm_bled_avg_sim_per_area_alphafixed.pdf')
plt.savefig(plot_dir+'/bsipm_bled_avg_sim_per_area_alphafixed.png',dpi=300)
plt.close()


#reflectances=['0.9','0.91','0.92','0.93','0.94','0.95','0.96','0.97','0.98','0.99']
#bestfits = results[0]
#all_chi2s = results[1]
#best1 = results[0][0][0]
#best2 = results[0][0][1]
#best3 = results[0][0][2]
#best4 = results[0][0][3]
#
#plot_xs = [float(item) for item in reflectances]
#interp_xs = np.arange(min(plot_xs),max(plot_xs),0.0001)
#
#curve1_xs = list(it.product(reflectances,[best2],[best3],[best4]))
#curve1_ys = []
#for key in curve1_xs:
#    curve1_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve1_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[0] + ' ' + all_conditions[0] + ' ('+all_diodes[0]+','+all_sipms[0]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[0]+'.png',dpi=300)
#minval1 = min(curve1_ys)
#rescaled_ys1 = [y/minval1 for y in curve1_ys]
#interp_ys1 = np.interp(interp_xs,plot_xs,rescaled_ys1)
#bound_range1 = [elt[0] for elt in zip(interp_xs,interp_ys1) if elt[1] <= 2]
#upperbound1 = max(bound_range1)
#lowerbound1 = min(bound_range1)
#
#curve2_xs = list(it.product([best1],reflectances,[best3],[best4]))
#curve2_ys = []
#for key in curve2_xs:
#    curve2_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve2_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[1] + ' ' + all_conditions[1] + ' ('+all_diodes[1]+','+all_sipms[1]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[1]+'.png',dpi=300)
#minval2 = min(curve2_ys)
#rescaled_ys2 = [y/minval2 for y in curve2_ys]
#interp_ys2 = np.interp(interp_xs,plot_xs,rescaled_ys2)
#bound_range2 = [elt[0] for elt in zip(interp_xs,interp_ys2) if elt[1] <= 2]
#upperbound2 = max(bound_range2)
#lowerbound2 = min(bound_range2)
#
#curve3_xs = list(it.product([best1],[best2],reflectances,[best4]))
#curve3_ys = []
#for key in curve3_xs:
#    curve3_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve3_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[2] + ' ' + all_conditions[2] + ' ('+all_diodes[2]+','+all_sipms[2]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[2]+'.png',dpi=300)
#minval3 = min(curve3_ys)
#rescaled_ys3 = [y/minval3 for y in curve3_ys]
#interp_ys3 = np.interp(interp_xs,plot_xs,rescaled_ys3)
#bound_range3 = [elt[0] for elt in zip(interp_xs,interp_ys3) if elt[1] <= 2]
#upperbound3 = max(bound_range3)
#lowerbound3 = min(bound_range3)
#
#curve4_xs = list(it.product([best1],[best2],[best3],reflectances))
#curve4_ys = []
#for key in curve4_xs:
#    curve4_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve4_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[3] + ' ' + all_conditions[3] + ' ('+all_diodes[3]+','+all_sipms[3]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[3]+'.png',dpi=300)
#minval4 = min(curve4_ys)
#rescaled_ys4 = [y/minval4 for y in curve4_ys]
#interp_ys4 = np.interp(interp_xs,plot_xs,rescaled_ys4)
#bound_range4 = [elt[0] for elt in zip(interp_xs,interp_ys4) if elt[1] <= 2]
#upperbound4 = max(bound_range4)
#lowerbound4 = min(bound_range4)

#plt.figure()
#title = 'Simultaneous fit to all boxes (last 4 points only), Blue SiPMs with Blue C LED'
#ps.plot_simultaneous_fits(all_data,all_sim,other_results,bb_systs,1.,all_thicknesses,
#        all_conditions,title)
#plt.savefig(plot_dir+'/blue_sipm_blue_led_last4.png',dpi=300)
