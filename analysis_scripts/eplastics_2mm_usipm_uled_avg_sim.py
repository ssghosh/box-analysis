import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly
import csv
from scipy.optimize import minimize
from collections import namedtuple
import pickle
import itertools as it
from os import path
import sys

# append the parent directory to the python path...
sys.path.append(os.path.dirname(sys.path[0]))
# ...to enable importing plot_utils
import plot_utils as ps


plot_res = 300 #dpi

##### ESSENTIAL ANALYSIS INPUTS START HERE ######
# define the bias voltage used + vops for the four SiPMs
bias_v = 54.6 #V
blue_v_brs = [54.98, 54.52, 54.76, 54.69]
uv_v_brs = [-np.inf,55.95,56.34,56.40]

plot_dir = 'eplastics_2mm_usipm_uled_avg_sim'
data_dir = 'measured_data'

all_data, all_conditions, all_thicknesses, all_systs, all_sipms, all_diodes, all_config = ps.read_csv('../'+data_dir+'/uu_eplastics.csv',
						diode=['uv'],
						sipm=['3uv-1blue'],
						vbias = bias_v,
						vops = uv_v_brs,
						summing_data = True)



frac_systs = pickle.load(open('./systematics/bb_frac_systs.pkl','rb'))
all_systs = ps.syst_from_frac_syst(frac_systs,all_data,
                                  all_thicknesses,
                                  all_conditions)
bb_systs = pickle.load(open('./systematics/bb_systs.pkl','rb'))

biglist = []
for key in all_systs:
    biglist.extend(all_systs[key])
#biglist = np.array(biglist)
#print('>>> average of all_systs is',np.mean(biglist))

poisson_biglist = []
for dataset in all_data:
    poisson_biglist.extend(dataset['Poisson err (nWb)'])
#print('>>> average of poisson errors is',np.mean(poisson_biglist))



#### ESSENTIAL ANALYSIS INPUTS END HERE ######

## read in simulation data
uv_sim = ps.read_sim('simulation/lambertian_1mil_allpcts_uv/outuv.dat')
blue_sim = ps.read_sim('simulation/lambertian_1mil_allpcts/outblue.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/blue_profiler_raisedSiPMs_1mil.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/near_far_blue_2mm_test_1M.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/blue_conserved_profile_1mil.dat')
#all_sim = ps.read_sim('simulation/consolidated_results/consolidated_results/uv_gen_and_match_1mil.dat')
#sim1 = ps.read_sim('simulation/consolidated_results/consolidated_results/custom_profile_uv_a_0p1in_0p2in_1mil.dat')
#sim2 = ps.read_sim('simulation/consolidated_results/consolidated_results/custom_profile_uv_c_0p1in_0p2in_1mil.dat')
sim1 = ps.read_sim('simulation/consolidated_results/consolidated_results/double_resolution_uv_a_0p1in_0p3in_80s_90s_1mil.dat')
sim2 = ps.read_sim('simulation/consolidated_results/consolidated_results/double_resolution_uv_c_0p1in_0p3in_80s_90s_1mil.dat')
avg_sim, simerr, syst_err_diffs = ps.avg_sim(sim1,sim2,1e6)
#print('>>> simerr is',simerr)



labels, handles = ps.plot_data(all_data,all_conditions,all_thicknesses,
        all_diodes,all_sipms,label_contents=['thickness'])
plt.legend(handles,labels)
plt.savefig(plot_dir+'/data_comparison.png')

results = []
if path.exists(plot_dir+'/fit_results.pkl'):
    results = pickle.load(open(plot_dir+'/fit_results.pkl', 'rb'))
else:
    results = ps.simultaneous_fit_data(all_data,avg_sim,
                                       dict(all_systs),
                                       1.,
                                       all_thicknesses,
                                       all_conditions,
                                       simerr=simerr)
    pickle.dump(results,open(plot_dir+'/fit_results.pkl','wb'))

independent_fit_results = ps.fit_all_data(all_data,avg_sim,dict(all_systs),1.,all_thicknesses,
                                          all_conditions,simerr=simerr)


fit_degree = 4
interval = 6 
chi2_scans, poly_fits, chi2_windows = ps.chi2_curves(results,all_data,dict(all_systs),
                                       avg_sim,1.,all_thicknesses,
                                       all_conditions,simerr,
                                       fit_degree=fit_degree,
                                       interval=interval)
#print('>>> type(poly_fits[0]) is',type(poly_fits[0]))
#def polynomial(x,coeffs,deg=4):
#    result = 0
#    #print('>>> coeffs are',coeffs)
#    for idx,exp in enumerate(range(deg,0,-1)):
#        #print('>>> adding x^',exp,'*',coeffs[idx])
#        result += (x**exp)*coeffs[idx]
#    return result
plt.close()

for idx, scan in enumerate(chi2_scans):
    reflectances = [float(item) for item in chi2_windows[idx]]
    plotting_xs = np.arange(min(reflectances),max(reflectances),0.001)
    #fit_ys = [polynomial(x,poly_fits[0],deg=fit_degree) for x in plotting_xs]
    fit_ys = poly.polyval(plotting_xs,poly_fits[idx])

    plt.figure()
    plt.plot(sorted([float(item) for item in chi2_windows[idx]]),chi2_scans[idx],
             marker = 'o',label='Calculated $\\chi^{2}$s')
    plt.plot(plotting_xs,fit_ys,marker='',label='Polynomial fit (degree = '+str(fit_degree)+')')
    plt.title(all_thicknesses[idx])
    plt.xlabel('Reflectance')
    plt.ylabel('$\\chi^{2}$/d.o.f.')
    plt.legend()
    plt.savefig(plot_dir+'/chi2_curve_'+all_thicknesses[idx]+'.png',dpi=300)
    plt.close()
         
#reflectances=['0.9','0.91']
#reflectances=['0.9','0.91','0.92','0.93','0.94','0.95','0.96','0.97','0.98','0.99']
reflectances=['0.8','0.805','0.81','0.815','0.82','0.825','0.83','0.835',
              '0.84','0.845','0.85','0.855','0.86','0.865','0.87','0.875',
              '0.88','0.885','0.89','0.895',
              '0.9','0.905','0.91','0.915','0.92','0.925','0.93','0.935',
              '0.94','0.945','0.95','0.955','0.96','0.965','0.97','0.975',
              '0.98','0.985','0.99','0.995']
bestfits = results[0]
all_chi2s = results[1]
best1 = results[0][0][0]
best2 = results[0][0][1]
best3 = results[0][0][2]
best4 = results[0][0][3]

plot_xs = [float(item) for item in reflectances]
interp_xs = np.arange(min(plot_xs),max(plot_xs),0.0001)

curve1_xs = list(it.product(reflectances,[best2],[best3],[best4]))
curve1_ys = []
for key in curve1_xs:
    curve1_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve1_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[0] + ' ' + all_conditions[0] + ' ('+all_diodes[0]+' LED, UV SiPMs)')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[0]+'.png',dpi=300)
minval1 = min(curve1_ys)
rescaled_ys1 = [y/minval1 for y in curve1_ys]
interp_ys1 = np.interp(interp_xs,plot_xs,rescaled_ys1)
bound_range1 = [elt[0] for elt in zip(interp_xs,interp_ys1) if elt[1] <= 2]
upperbound1 = max(bound_range1)
lowerbound1 = min(bound_range1)

curve2_xs = list(it.product([best1],reflectances,[best3],[best4]))
curve2_ys = []
for key in curve2_xs:
    curve2_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve2_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[1] + ' ' + all_conditions[1] + ' ('+all_diodes[1]+' LED, UV SiPMs)')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[1]+'.png',dpi=300)
minval2 = min(curve2_ys)
rescaled_ys2 = [y/minval2 for y in curve2_ys]
interp_ys2 = np.interp(interp_xs,plot_xs,rescaled_ys2)
bound_range2 = [elt[0] for elt in zip(interp_xs,interp_ys2) if elt[1] <= 2]
upperbound2 = max(bound_range2)
lowerbound2 = min(bound_range2)

curve3_xs = list(it.product([best1],[best2],reflectances,[best4]))
curve3_ys = []
for key in curve3_xs:
    curve3_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve3_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[2] + ' ' + all_conditions[2] + ' ('+all_diodes[2]+' LED, UV SiPMs)')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[2]+'.png',dpi=300)
minval3 = min(curve3_ys)
rescaled_ys3 = [y/minval3 for y in curve3_ys]
interp_ys3 = np.interp(interp_xs,plot_xs,rescaled_ys3)
bound_range3 = [elt[0] for elt in zip(interp_xs,interp_ys3) if elt[1] <= 2]
upperbound3 = max(bound_range3)
lowerbound3 = min(bound_range3)

curve4_xs = list(it.product([best1],[best2],[best3],reflectances))
curve4_ys = []
for key in curve4_xs:
    curve4_ys.append(all_chi2s[key])
plt.figure()
plt.plot(plot_xs,curve4_ys)
plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[3] + ' ' + all_conditions[3] + ' ('+all_diodes[3]+' LED, UV SiPMs)')
plt.xlabel('Reflectance')
plt.ylabel(r'$\chi^2$')
plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[3]+'.png',dpi=300)
minval4 = min(curve4_ys)
rescaled_ys4 = [y/minval4 for y in curve4_ys]
interp_ys4 = np.interp(interp_xs,plot_xs,rescaled_ys4)
bound_range4 = [elt[0] for elt in zip(interp_xs,interp_ys4) if elt[1] <= 2]
upperbound4 = max(bound_range4)
lowerbound4 = min(bound_range4)

old_ref_bounds = [(lowerbound1,upperbound1),
                  (lowerbound2,upperbound2),
                  (lowerbound3,upperbound3),
                  (lowerbound4,upperbound4)]

old_chi2_ys = [rescaled_ys1,rescaled_ys2,rescaled_ys3,rescaled_ys4]

# make comparison plots of the old and new chi2 curves
all_ref_bounds = []
deriv_ref_bounds = []
for idx, new_curve in enumerate(chi2_scans):
    old_chi2s_in_window = []
    #print('>>> old_chi2_ys is',old_chi2_ys)
    #print('>>> type(old_chi2_ys) is',type(old_chi2_ys))
    # get the reflectance window first
    #print('>>> old_chi2_ys[idx] is',old_chi2_ys[idx])
    #print('>>> chi2_windows[idx] is',chi2_windows[idx])
    for jdx, element in enumerate(old_chi2_ys[idx]):
        if reflectances[jdx] in chi2_windows[idx]:
            old_chi2s_in_window.append(element)
    #print('>>> old_chi2s_in_window is',old_chi2s_in_window)

    # figure out ref bounds using new chi2 curves
    interp_xs = np.arange(float(min(chi2_windows[idx])),
                          float(max(chi2_windows[idx])),
                          0.0001)
    interp_ys = np.interp(interp_xs,chi2_windows[idx],new_curve)
    bound_range = [elt[0] for elt in zip(interp_xs,interp_ys) if elt[1] <= 2]
    upperbound = max(bound_range)
    lowerbound = min(bound_range)
    all_ref_bounds.append((lowerbound,upperbound))

    # figure out ref bounds using derivative
    deriv = np.polyder(poly_fits[idx],2)
    min_idx = np.argmin(new_curve)
    min_ref = float(chi2_windows[idx][min_idx])
    der_at_min = poly.polyval(min_ref,deriv)
    err_from_der = 1/np.sqrt(0.5*der_at_min)
    deriv_ref_bounds.append(err_from_der)


    plt.figure()
    plt.plot(chi2_windows[idx],new_curve,
             linestyle='--',
             marker='o',
             label='New $\\chi^{2}$ ($\\alpha$ fixed)')
    plt.plot(chi2_windows[idx],old_chi2s_in_window,
             linestyle='--',
             marker='o',
             label='Old $\\chi^{2}$ ($\\alpha$ changing)')
    plt.title(all_thicknesses[idx])
    plt.xlabel('Reflectance')
    plt.ylabel('$\\chi^{2}$')
    plt.legend()
    plt.savefig(plot_dir+'/chi2_comparison_'+all_thicknesses[idx]+'.png',
                dpi=300)
    plt.close()

#print('>>> new all_ref_bounds is',all_ref_bounds)
#print('>>> deriv_ref_bounds are',deriv_ref_bounds)

ps.plot_residuals(all_data,avg_sim,results,all_systs,1.,all_thicknesses,
        all_conditions,simerr=simerr,total_area=27,ylim=[-0.3,0.3])
plt.savefig(plot_dir+'/usipm_uled_residuals.pdf')

# get average of (scaled!) stat error for sim1 and sim2
winning_mcs = results[0][0]
#print('>>> winning_mcs is',winning_mcs)
winning_alpha = results[0][1]
N = 1e6
sim1_errs = []
for key in sim1.keys():
    if key in winning_mcs:
        for n in sim1[key][1]:
            err = np.sqrt(n*(1-n/N))
            err_nwb = err*winning_alpha
            sim1_errs.append(err_nwb)
sim1_avg_err = np.mean(sim1_errs)
#print('>>> sim1_avg_err is',sim1_avg_err,'nWb')

sim2_errs = []
for key in sim2.keys():
    if key in winning_mcs:
        for n in sim2[key][1]:
            err = np.sqrt(n*(1-n/N))
            err_nwb = err*winning_alpha
            sim2_errs.append(err_nwb)
sim2_avg_err = np.mean(sim2_errs)
#print('>>> sim2_avg_err is',sim2_avg_err,'nWb')

total_errs = []
for key in simerr:
    if key in winning_mcs:
        for err in simerr[key][1]:
            err_nwb = err*winning_alpha
            total_errs.append(err_nwb)
total_avg_err = np.mean(total_errs)

total_syst_err_diffs = []
for key in simerr:
    if key in winning_mcs:
        for err in syst_err_diffs[key][1]:
            err_nwb = err*winning_alpha
            total_syst_err_diffs.append(err_nwb)
total_avg_syst_err_diffs = np.mean(total_syst_err_diffs)
            
            
#print('>>> total average err is',total_avg_err,'nWb')
#print('>>> diff with sim1 err:',total_avg_err-sim1_avg_err)
#print('>>> diff with sim2 err:',total_avg_err-sim2_avg_err)
#print('>>> avg systematic contribution is:',total_avg_syst_err_diffs)




plt.figure()
title = 'UV LED with UV SiPMs \n $\\chi^{2} \\leq 2$ bounds ($\\alpha$ changing)'
#print('>>> simerr is',simerr)
total_area = 27 #mm^2
#print('>>> all_data[0][Distance (mm)] is',all_data[0]['Distance (mm)'])
ps.plot_simultaneous_fits(all_data,avg_sim,results,bb_systs,1.,
        all_thicknesses,all_conditions,title,
        simerr=simerr,ref_bounds=old_ref_bounds,total_area=total_area,
        title_fontsize=15)
plt.savefig(plot_dir+'/usipm_uled_avg_sim_per_area_alphachanging.png',dpi=300)
plt.close()

plt.close('all')
plt.figure()
title = 'Test: Non-simultaneous UV LED/UV SiPM fit'
total_area = 27 #mm^2
labels, handles = ps.plot_fits(all_data,avg_sim,independent_fit_results[1],all_conditions,all_thicknesses,
             all_diodes,all_sipms,systs=all_systs,xerr=1.,label_contents=['thickness'],
             total_area=total_area)
plt.legend(handles,labels,prop={'size':9},framealpha=0.3)
plt.title(title)
plt.savefig(plot_dir+'/test_non_simultaneous_uled_usipm_fit.png',dpi=300)

print('>>> SAVED PLOT OF NON-SIMULTANEOUS FITS')
plt.close()
             

plt.figure()
title = 'UV LED with UV SiPMs \n $\\chi^{2} \\leq 2$ bounds ($\\alpha$ fixed)'
title = 'UV LED with UV SiPMs'
#print('>>> simerr is',simerr)
total_area = 27 #mm^2
#print('>>> all_data[0][Distance (mm)] is',all_data[0]['Distance (mm)'])
ordinary_fontsize = 17
ps.plot_simultaneous_fits(all_data,avg_sim,results,all_systs,1.,
        all_thicknesses,all_conditions,'',#title,
        simerr=simerr,ref_bounds=all_ref_bounds,total_area=total_area,
        #title_fontsize=15,label_fontsize=15,all_fontsizes=15,ylim=[1.1,5])
        title_fontsize=ordinary_fontsize,label_fontsize=ordinary_fontsize,
        legend_fontsize=14,ticklabel_fontsize=ordinary_fontsize,
        infotext_fontsize=ordinary_fontsize,pad=0.3,legalpha=0.4)
plt.savefig(plot_dir+'/usipm_uled_avg_sim_per_area_alphafixed.pdf')
plt.savefig(plot_dir+'/usipm_uled_avg_sim_per_area_alphafixed.png',dpi=300)
plt.close()

# test out the residuals and fit on same plot
plt.figure(figsize=(5,30))
title = 'UV LED with UV SiPMs \n $\\chi^{2} \\leq 2$ bounds ($\\alpha$ fixed)'
title = 'UV LED with UV SiPMs'
#print('>>> simerr is',simerr)
total_area = 27 #mm^2
#print('>>> all_data[0][Distance (mm)] is',all_data[0]['Distance (mm)'])
ordinary_fontsize = 17
ps.plot_simultaneous_fits(all_data,avg_sim,results,all_systs,1.,
        all_thicknesses,all_conditions,'',#title,
        simerr=simerr,ref_bounds=all_ref_bounds,total_area=total_area,
        #title_fontsize=15,label_fontsize=15,all_fontsizes=15,ylim=[1.1,5])
        title_fontsize=ordinary_fontsize,label_fontsize=ordinary_fontsize,
        legend_fontsize=14,ticklabel_fontsize=ordinary_fontsize,
        infotext_fontsize=ordinary_fontsize,pad=0.3,legalpha=0.4,
        residuals=True,res_ylim=[-0.3,0.3])
plt.savefig(plot_dir+'/unified_usipm_uled_avg_sim_per_area_alphafixed.pdf')
plt.close()

plt.figure()
title = 'UV LED with UV SiPMs \n $1/\\sqrt{0.5 \\frac{d^{2}\\chi^{2}}{dR^{2}}}$ bounds'
#print('>>> simerr is',simerr)
total_area = 27 #mm^2
#print('>>> all_data[0][Distance (mm)] is',all_data[0]['Distance (mm)'])
ps.plot_simultaneous_fits(all_data,avg_sim,results,bb_systs,1.,
        all_thicknesses,all_conditions,title,
        simerr=simerr,ref_bounds=deriv_ref_bounds,total_area=total_area,
        title_fontsize=10)
plt.savefig(plot_dir+'/usipm_uled_avg_sim_per_area_der_errs.pdf')

#reflectances=['0.9','0.91','0.92','0.93','0.94','0.95','0.96','0.97','0.98','0.99']
#bestfits = results[0]
#all_chi2s = results[1]
#best1 = results[0][0][0]
#best2 = results[0][0][1]
#best3 = results[0][0][2]
#best4 = results[0][0][3]
#
#plot_xs = [float(item) for item in reflectances]
#interp_xs = np.arange(min(plot_xs),max(plot_xs),0.0001)
#
#curve1_xs = list(it.product(reflectances,[best2],[best3],[best4]))
#curve1_ys = []
#for key in curve1_xs:
#    curve1_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve1_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[0] + ' ' + all_conditions[0] + ' ('+all_diodes[0]+','+all_sipms[0]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[0]+'.png',dpi=300)
#minval1 = min(curve1_ys)
#rescaled_ys1 = [y/minval1 for y in curve1_ys]
#interp_ys1 = np.interp(interp_xs,plot_xs,rescaled_ys1)
#bound_range1 = [elt[0] for elt in zip(interp_xs,interp_ys1) if elt[1] <= 2]
#upperbound1 = max(bound_range1)
#lowerbound1 = min(bound_range1)
#
#curve2_xs = list(it.product([best1],reflectances,[best3],[best4]))
#curve2_ys = []
#for key in curve2_xs:
#    curve2_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve2_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[1] + ' ' + all_conditions[1] + ' ('+all_diodes[1]+','+all_sipms[1]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[1]+'.png',dpi=300)
#minval2 = min(curve2_ys)
#rescaled_ys2 = [y/minval2 for y in curve2_ys]
#interp_ys2 = np.interp(interp_xs,plot_xs,rescaled_ys2)
#bound_range2 = [elt[0] for elt in zip(interp_xs,interp_ys2) if elt[1] <= 2]
#upperbound2 = max(bound_range2)
#lowerbound2 = min(bound_range2)
#
#curve3_xs = list(it.product([best1],[best2],reflectances,[best4]))
#curve3_ys = []
#for key in curve3_xs:
#    curve3_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve3_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[2] + ' ' + all_conditions[2] + ' ('+all_diodes[2]+','+all_sipms[2]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[2]+'.png',dpi=300)
#minval3 = min(curve3_ys)
#rescaled_ys3 = [y/minval3 for y in curve3_ys]
#interp_ys3 = np.interp(interp_xs,plot_xs,rescaled_ys3)
#bound_range3 = [elt[0] for elt in zip(interp_xs,interp_ys3) if elt[1] <= 2]
#upperbound3 = max(bound_range3)
#lowerbound3 = min(bound_range3)
#
#curve4_xs = list(it.product([best1],[best2],[best3],reflectances))
#curve4_ys = []
#for key in curve4_xs:
#    curve4_ys.append(all_chi2s[key])
#plt.figure()
#plt.plot(plot_xs,curve4_ys)
#plt.title(r'Simultaneous $\chi^2$ vs fit reflectance for '+all_thicknesses[3] + ' ' + all_conditions[3] + ' ('+all_diodes[3]+','+all_sipms[3]+')')
#plt.xlabel('Reflectance')
#plt.ylabel(r'$\chi^2$')
#plt.savefig(plot_dir+'/blue_sipm_blue_led_'+all_thicknesses[3]+'.png',dpi=300)
#minval4 = min(curve4_ys)
#rescaled_ys4 = [y/minval4 for y in curve4_ys]
#interp_ys4 = np.interp(interp_xs,plot_xs,rescaled_ys4)
#bound_range4 = [elt[0] for elt in zip(interp_xs,interp_ys4) if elt[1] <= 2]
#upperbound4 = max(bound_range4)
#lowerbound4 = min(bound_range4)

#plt.figure()
#title = 'Simultaneous fit to all boxes (last 4 points only), Blue SiPMs with Blue C LED'
#ps.plot_simultaneous_fits(all_data,all_sim,other_results,bb_systs,1.,all_thicknesses,
#        all_conditions,title)
#plt.savefig(plot_dir+'/blue_sipm_blue_led_last4.png',dpi=300)

