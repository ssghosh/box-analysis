import sys
sys.path.append('../')
import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy.optimize import minimize
from collections import namedtuple
import plot_utils as ps
import pickle
import itertools as it
from os import path
plot_res = 300 #dpi

file_list = ['bsipm_bled_run1.csv',
             'bsipm_bled_run2.csv',
             'bsipm_bled_run3.csv',
             'bsipm_bled_run4.csv']
##### ESSENTIAL ANALYSIS INPUTS START HERE ######
# define the bias voltage used + vops for the four SiPMs
bias_v = 54.6 #V
blue_v_brs = [54.98, 54.52, 54.76, 54.69]
uv_v_ops = [-np.inf,55.95,56.34,56.40]


def calculate_systs(file_list,bias_v=None,v_brs=None):
    file_data = {}
    file_conditions = {}
    file_thicknesses = {}
    file_sipms = {}
    file_leds = {}
    for filename in file_list:
        print('>>> reading out',filename)
        all_data, all_conditions, all_thicknesses, all_systs, all_sipms, all_diodes, all_configs = ps.read_csv(filename,
                                diode=['blue c','uv'],
                                sipm=['4blue','3uv - 1blue'],
                                vbias = bias_v,
                                vops = blue_v_brs,
                                summing_data = True)
        file_data[filename] = all_data
        file_conditions[filename] = all_conditions
        file_thicknesses[filename] = all_thicknesses
        file_sipms[filename] = all_sipms
        file_leds[filename] = all_diodes

    # group data by configuration and insertion distance...
    grouped = {}
    for condition in file_conditions[file_list[0]]:
        for thickness in file_thicknesses[file_list[0]]:
            for sipm in file_sipms[file_list[0]]:
                for led in file_leds[file_list[0]]:
                    grouped[(condition,thickness,sipm,led)] = []

    for filename in file_list:
        for key in grouped.keys():
            cond_indxs = [i for i,x in enumerate(file_conditions[filename]) if x == key[0]]
            thick_indxs = [i for i,x in enumerate(file_thicknesses[filename]) if x == key[1]]
            sipm_indxs = [i for i,x in enumerate(file_sipms[filename]) if x == key[2]]
            led_indxs = [i for i,x in enumerate(file_leds[filename]) if x == key[3]]

            this_idx = list(set(cond_indxs).intersection(thick_indxs,sipm_indxs,led_indxs))[0]

            grouped[key].append(file_data[filename][this_idx]['Int (nWb)'])

    # then make the uncerts dict and put the stdevs in it
    # and also do a fractional systs dict
    systs = {}
    frac_systs = {}
    for key in grouped.keys():
        data = np.array(grouped[key])
        systs[(key[1],key[0])] = []
        frac_systs[(key[1],key[0])] = []
        for idx in range(len(data[0])):
            col = data[:,idx]
            stdev = np.std(col)
            mean = np.mean(col)
            systs[(key[1],key[0])].append(stdev)
            frac_systs[(key[1],key[0])].append(stdev/mean)

    return systs, frac_systs


bb_systs, bb_frac_systs = calculate_systs(file_list)
pickle.dump(bb_systs,open('bb_systs.pkl','wb'))
pickle.dump(bb_frac_systs,open('bb_frac_systs.pkl','wb'))



# sipm, led
#bb_systs = {('1 cm', 'uncoated'): [0.429,0.281,0.363,0.482,0.514,0.535,0.821,1.409],
#            ('8 mm', 'uncoated'): [0.381,0.376,0.236,0.242,0.335,0.374,0.424,0.961],
#            ('6 mm', 'uncoated'): [0.429,0.297,0.271,0.204,0.258,0.386,0.434,0.935],
#            ('5 mm', 'uncoated'): [0.298,0.275,0.326,0.293,0.416,0.374,0.373,1.039]}


#print('all_systs is',all_systs)
#print('all_data is',all_data)
#print('ub_systs is',ub_systs)

#ub_frac_systs = dict.fromkeys(bb_systs.keys())
#for idx, key in enumerate(zip(all_thicknesses,all_conditions)):
#    bb_dataset = all_data[idx]
#    this_bb_systs = bb_systs[key]
#    frac_systs = []
#    for jdx,signal in enumerate(bb_dataset['Int (nWb)']):
#        frac_systs.append(this_bb_systs[jdx]/signal)
#    ub_frac_systs[key] = frac_systs
#
#ub_systs = dict.fromkeys(bb_systs.keys())
#for idx, key in enumerate(zip(ub_thicknesses,ub_conditions)):
#    ub_dataset = ub_data[idx]
#    this_ub_frac_systs = ub_frac_systs[key]
#    ub_nwb_systs = []
#    for jdx,signal in enumerate(ub_dataset['Int (nWb)']):
#        ub_nwb_systs.append(this_ub_frac_systs[jdx]*signal)
#    ub_systs[key] = ub_nwb_systs
#
#simerrscale = 2.
#bb_simerrs = {}
#for key in bb_systs.keys():
#    syst = bb_systs[key]
#    new_syst = []
#    for item in syst:
#        new_syst.append(item*simerrscale)
#    bb_simerrs[key] = new_syst
#
#agg_avg_syst = []
#for key in bb_systs.keys():
#    syst = bb_systs[key]
#    for item in syst:
#        agg_avg_syst.append(item)
#
##print('>>> About to take a mean')
#avg_d_syst = np.mean(np.array(agg_avg_syst))
##print('>>> took a mean')
#single_bb_simerrs = {}
#for key in bb_systs.keys():
#    single_bb_simerrs[key] = avg_d_syst*np.ones(len(bb_systs[key]))
#
#ub_simerrs = {}
##print('ub_systs is',ub_systs)
#for key in ub_systs.keys():
#    syst = ub_systs[key]
#    new_syst = []
#    for item in syst:
#        new_syst.append(item*simerrscale)
#    ub_simerrs[key] = new_syst
#
#agg_avg_syst = []
#for key in ub_systs.keys():
#    syst = ub_systs[key]
#    for item in syst:
#        agg_avg_syst.append(item)
#
##print('>>> aobut to take a mean')
#avg_d_syst = np.mean(np.array(agg_avg_syst))
##print('>>> took a mean')
#single_ub_simerrs = {}
#for key in ub_systs.keys():
#    single_ub_simerrs[key] = avg_d_syst*np.ones(len(ub_systs[key]))
#    
#
